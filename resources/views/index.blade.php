@extends('layouts.app')

@section('content')
<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>    
    <!-- Hero Section Begin -->
    <section class="hero-section">
        <div class="hero-items owl-carousel">
            <div class="single-hero-items set-bg" data-setbg="{{url($slider[0]->s_img1)}}"  style="height:80%">
                <div class="container">
                    <div class="row p-4">
                        <div class="col-lg-4">
                            <span>Bag,kids</span>
                            <h1>{{$slider[0]->s_text1}}</h1>
                            <p>{{$slider[0]->s_content1}}</p>
                            <a href="#" class="primary-btn">Shop Now</a>
                        </div>
                    </div>
                    <!-- <div class="off-card">
                        <h2>Sale <span>50%</span></h2>
                    </div> -->
                </div>
            </div>
            <div class="single-hero-items set-bg" data-setbg="{{url($slider[0]->s_img2)}}" style="height:80%">
                <div class="container">
                    <div class="row p-4">
                        <div class="col-lg-5">
                            <span>Bag,kids</span>
                            <h1>{{$slider[0]->s_text2}}</h1>
                            <p>\{{$slider[0]->s_content2}}</p>
                            <a href="#" class="primary-btn">Shop Now</a>
                        </div>
                    </div>
                    <!-- <div class="off-card">
                        <h2>Sale <span>50%</span></h2>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- Banner Section Begin -->
    <div class="banner-section spad">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="single-banner">
                        <img src="{{ asset('images/img/banner-1.jpg')}}" alt="">
                        <div class="inner-text">
                            <h4>Led's</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-banner">
                        <img src="{{ asset('images/img/banner-2.jpg')}}" alt="">
                        <div class="inner-text">
                            <h4>Freez's</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-banner">
                        <img src="{{ asset('images/img/banner-3.jpg')}}" alt="">
                        <div class="inner-text">
                            <h4>Fan's</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner Section End -->

    <!-- Women Banner Section Begin -->
    <section class="women-banner spad">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3">
                    <div class="product-large set-bg" data-setbg="{{url($sideimg[0]->l_img)}}">
                        <h2>Latest</h2>
                        <a href="#">Discover More</a>
                    </div>
                </div>
                <div class="col-lg-8 offset-lg-1">
                    <div class="filter-control">
                        <ul>
                            @foreach($category as $item)
                            <li value="{{$item->id}}">{{$item->name}}</li>     
                            @endforeach
                        </ul>
                    </div>
                          <div class="product-slider owl-carousel">
                                    @foreach($products as $product)
                                    <div class="product-item">
                                        <div class="pi-pic">
                                            <img src="{{url($product->cover_photo)}}" alt="">
                                            <div class="sale">Sale</div>
                                            <div class="icon">
                                                <i class="icon_heart_alt"></i>
                                            </div>
                                            <ul>
                                                <li class="w-icon active"><a href="#"><i class="icon_bag_alt"></i></a></li>
                                                <li class="quick-view"><a href="product/{{$product->id}}">Shop Now</a></li>
                                                <li class="w-icon"><a href="#"><i class="fa fa-random"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="pi-text">
                                            <div class="catagory-name">Coat</div>
                                            <a href="#">
                                                <h5>{{$product->name}}</h5>
                                            </a>
                                            <div class="product-price">
                                                $14.00
                                                <span>$35.00</span>
                                            </div>
                                        </div>
                                    </div>  
                                    @endforeach                                                      
                       </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Women Banner Section End -->

    <!-- Deal Of The Week Section Begin-->
    <section class="deal-of-week set-bg spad" data-setbg="{{url($data->image)}}">
        <div class="container">
            <div class="col-lg-6 text-center">
                <div class="section-title">
                    <h2>Deal Of The Week</h2>
                    <p>{{$data->content}}</p>
                    <div class="product-price">
                        $35.00
                        <span>/ HanBag</span>
                    </div>
                </div>
                <div class="countdown-timer" id="countdownTimer">
                    
                </div>
                <a href="#" class="primary-btn" id="shop-btn">Shop Now</a>
            </div>
        </div>
    </section>
    <!-- Deal Of The Week Section End -->

    <!-- Man Banner Section Begin -->
    <section class="man-banner spad">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8">
                    <div class="filter-control">
                        <ul>
                            @foreach($category as $item)
                            <li value="{{$item->id}}">{{$item->name}}</li>     
                            @endforeach
                        </ul>
                    </div>
                    <div class="product-slider owl-carousel">
                        @foreach($products as $product)
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="{{url($product->cover_photo)}}" alt="">
                                <div class="sale">Sale</div>
                                <div class="icon">
                                    <i class="icon_heart_alt"></i>
                                </div>
                                <ul>
                                    <li class="w-icon active"><a href="#"><i class="icon_bag_alt"></i></a></li>
                                    <li class="quick-view"><a href="#">+ Quick View</a></li>
                                    <li class="w-icon"><a href="#"><i class="fa fa-random"></i></a></li>
                                </ul>
                            </div>
                            <div class="pi-text">
                                <div class="catagory-name">Coat</div>
                                <a href="#">
                                    <h5>{{$product->name}}</h5>
                                </a>
                                <div class="product-price">
                                    $14.00
                                    <span>$35.00</span>
                                </div>
                            </div>
                        </div>
                        @endforeach                        
                    </div>
                </div>
                <div class="col-lg-3 offset-lg-1">
                    <div class="product-large set-bg m-large" data-setbg="{{url($sideimg[0]->r_img)}}">
                        <h2>Feature</h2>
                        <a href="#">Discover More</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Man Banner Section End -->

    <!-- Instagram Section Begin -->
    <!-- <div class="instagram-photo">
        <div class="insta-item set-bg" data-setbg="{{ asset('images/img/insta-1.jpg')}}">
            <div class="inside-text">
                <i class="ti-instagram"></i>
                <h5><a href="#">colorlib_Collection</a></h5>
            </div>
        </div>
        <div class="insta-item set-bg" data-setbg="{{ asset('images/img/insta-2.jpg')}}">
            <div class="inside-text">
                <i class="ti-instagram"></i>
                <h5><a href="#">colorlib_Collection</a></h5>
            </div>
        </div>
        <div class="insta-item set-bg" data-setbg="{{ asset('images/img/insta-3.jpg')}}">
            <div class="inside-text">
                <i class="ti-instagram"></i>
                <h5><a href="#">colorlib_Collection</a></h5>
            </div>
        </div>
        <div class="insta-item set-bg" data-setbg="{{ asset('images/img/insta-4.jpg')}}">
            <div class="inside-text">
                <i class="ti-instagram"></i>
                <h5><a href="#">colorlib_Collection</a></h5>
            </div>
        </div>
        <div class="insta-item set-bg" data-setbg="{{ asset('images/img/insta-5.jpg')}}">
            <div class="inside-text">
                <i class="ti-instagram"></i>
                <h5><a href="#">colorlib_Collection</a></h5>
            </div>
        </div>
        <div class="insta-item set-bg" data-setbg="{{ asset('images/img/insta-6.jpg')}}">
            <div class="inside-text">
                <i class="ti-instagram"></i>
                <h5><a href="#">colorlib_Collection</a></h5>
            </div>
        </div>
    </div> -->
        <div class="row">
           <div class="col-md-1"></div>
           <div class="col-md-10 ">
                 <div class="product-slider owl-carousel">
                    @foreach($products as $product)
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="{{url($product->cover_photo)}}" alt="">
                                <div class="sale">Sale</div>
                                <div class="icon tect">
                                    <i class="icon_heart_alt"></i>
                                </div>
                                <ul>
                                    <li class="w-icon active"><a href="#"><i class="icon_bag_alt"></i></a></li>
                                    <li class="quick-view"><a href="#">+ Quick View</a></li>
                                    <li class="w-icon"><a href="#"><i class="fa fa-random"></i></a></li>
                                </ul>
                            </div>
                            <div class="pi-text">
                                <div class="catagory-name">Coat</div>
                                <a href="#">
                                    <h5>{{$product->name}}</h5>
                                </a>
                                <div class="product-price">
                                    $14.00
                                    <span>$35.00</span>
                                </div>
                            </div>
                        </div>
                        @endforeach                        
                    </div>
                </div>
            </div>
    <!-- Instagram Section End -->

    <!-- Latest Blog Section Begin -->
    <section class="latest-blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>From The Blog</h2>
                    </div>
                </div>
            </div>
            <div class="row">                
                @foreach($post as $post)
                <div class="col-lg-4 col-md-6">
                    <div class="single-latest-blog">
                        <img src="{{url($post->image)}}" alt="" height="350">
                        <div class="latest-text">
                            <div class="tag-list">
                                <div class="tag-item">
                                    <i class="fa fa-calendar-o"></i>
                                    {{$post->created_at->format('d M Y')}}
                                </div>
                                @if($post->comment_count > 0)
                                <div class="tag-item">
                                    <i class="fa fa-comment-o"></i>     
                                    {{$post->comment_count}}
                                </div>
                                @endif
                            </div>
                            <a href="#">
                                <h4>{{$post->caption}}</h4>
                            </a>
                            <p>{{$post->content}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="benefit-items">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="single-benefit">
                            <div class="sb-icon">
                                <img src="{{ asset('images/img/icon-1.png')}}" alt="">
                            </div>
                            <div class="sb-text">
                                <h6>Free Shipping</h6>
                                <p>For all order over 99$</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="single-benefit">
                            <div class="sb-icon">
                                <img src="{{ asset('images/img/icon-2.png')}}" alt="">
                            </div>
                            <div class="sb-text">
                                <h6>Delivery On Time</h6>
                                <p>If good have prolems</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="single-benefit">
                            <div class="sb-icon">
                                <img src="{{ asset('images/img/icon-3.png')}}" alt="">
                            </div>
                            <div class="sb-text">
                                <h6>Secure Payment</h6>
                                <p>100% secure payment</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>  
<script>
var countDownDatestart = new Date("{{$data->start_date}}").getTime();
var countDownDateend = new Date("{{$data->end_date}}").getTime();

var x = setInterval(function() {
var now = new Date().getTime();
var distance1 = countDownDatestart - now;
var distance2 = countDownDateend - now;
// var days = Math.floor(distance / (1000 * 60 * 60 * 24));
var days1 = Math.floor(distance1 / (1000 * 60 * 60 * 24));
var hours1 = Math.floor((distance1 % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
var minutes1 = Math.floor((distance1 % (1000 * 60 * 60)) / (1000 * 60));
var seconds1 = Math.floor((distance1 % (1000 * 60)) / 1000);
var days2 = Math.floor(distance2 / (1000 * 60 * 60 * 24));
var hours2 = Math.floor((distance2 % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
var minutes2 = Math.floor((distance2 % (1000 * 60 * 60)) / (1000 * 60));
var seconds2 = Math.floor((distance2 % (1000 * 60)) / 1000);
// document.getElementById("demo1").innerHTML = hours1 + "h "+ minutes1 + "m " + seconds1 + "s ";
document.getElementById("countdownTimer").innerHTML = "<div class='cd-item'><span>"+ days2 +"</span><p>Days</p></div><div class='cd-item'><span>"+hours2+"</span><p>Hrs</p></div><div class='cd-item'><span>"+minutes2+"</span><p>Mins</p></div><div class='cd-item'><span>"+seconds2+"</span><p>Secs</p></div>";
if (distance2 < 0) {
clearInterval(x);
document.getElementById("countdownTimer").innerHTML = "EXPIRED";
document.getElementById("shop-btn").setAttribute("disabled", "disabled");

}
}, 1000);
</script>
@endsection