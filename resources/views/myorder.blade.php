@extends('layouts.app')

@section('content')
<style>
    .padding {
    padding: 2rem !important
}
.card {
    margin-bottom: 30px;
    border: none;
    -webkit-box-shadow: 0px 1px 2px 1px rgba(154, 154, 204, 0.22);
    -moz-box-shadow: 0px 1px 2px 1px rgba(154, 154, 204, 0.22);
    box-shadow: 0px 1px 2px 1px rgba(154, 154, 204, 0.22)
}

.card-header {
    background-color: #fff;
    border-bottom: 1px solid #e6e6f2
}

h3 {
    font-size: 20px
}

h5 {
    font-size: 15px;
    line-height: 26px;
    color: #3d405c;
    margin: 0px 0px 15px 0px;
    font-family: 'Circular Std Medium'
}

.text-dark {
    color: #3d405c !important
}
</style>
 <main class="py-4 bg-dark">
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>
    <div class="offset-xl-2 col-xl-8">
     <div class="card">
         <div class="card-header p-4">
             <a class="pt-2 d-inline-block" href="index.html" data-abc="true">Singhsolution.com</a>
             <div class="float-right">
                 <h3 class="mb-0">Invoice #BBB10234</h3>
                 Date: 12 Jun,201
             </div>
         </div>
         <div class="card-body">
             <div class="row mb-4">
                 <div class="col-sm-6">
                     <h5 class="mb-3">From:</h5>
                     <h3 class="text-dark mb-1">Balwant Singh</h3>
                     <div>New Road</div>
                     <div>Nathdwara,Rajsthan 313301</div>
                     <div>Email: contact@singhsolution.com</div>
                     <div>Phone: +91 6378707597</div>
                 </div>
                 <div class="col-sm-6 ">
                     <h5 class="mb-3">To:</h5>
                     <h3 class="text-dark mb-1">{{Auth::user()->name}}</h3>
                     <div>{{$order[0]->orderAddress[0]->street}}</div>
                     <div>{{$order[0]->orderAddress[0]->city}}, {{$order[0]->orderAddress[0]->zip}}</div>
                     <div>Email:{{$order[0]->orderAddress[0]->email}}</div>
                     <div>Phone: {{$order[0]->orderAddress[0]->phon}}</div>
                 </div>
             </div>
             <div class="table-responsive-sm">
                 <table class="table table-striped">
                     <thead>
                         <tr>
                             <th class="center">OrderId:</th>
                             <th>Item</th>
                             <th>Description</th>
                             <th class="right">Price</th>
                             <th class="center">Qty</th>
                             <th class="right">Total</th>
                             <th class="right">status</th>
                         </tr>
                     </thead>
                     <tbody>
                        @foreach($order as $item)
                         <tr>
                             <td class="center">000{{$item->id}}</td>
                             <td class="left strong">{{$item->product[0]->name}}</td>
                             <td class="left">{{$item->product[0]->description}}</td>
                             <td class="right">&#8377;{{$item->product[0]->price}}</td>
                             <td class="center">{{$item->qty}}</td>
                             <td class="right">&#8377;{{$item->gross_price}}</td>
                             <td class="right"><a href="{{url('ordertrack/')}}/{{$item->id}}"><button class="btn btn-success">Track Order</button></a></td>
                         </tr>
                         @endforeach                       
                     </tbody>
                 </table>
             </div>
             <div class="row">
                 <div class="col-lg-4 col-sm-5">
                 </div>
                 <div class="col-lg-4 col-sm-5 ml-auto">
                     <table class="table table-clear">
                         <tbody>
                             <tr>
                                 <td class="left">
                                     <strong class="text-dark">Subtotal</strong>
                                 </td>
                                 <td class="right">{{$total}}</td>
                             </tr>
                             <tr>
                                 <td class="left">
                                     <strong class="text-dark">Discount</strong>
                                 </td>
                                 <td class="right">/-</td>
                             </tr>
                             <tr>
                                 <td class="left">
                                     <strong class="text-dark">VAT</strong>
                                 </td>
                                 <td class="right">/-</td>
                             </tr>
                             <tr>
                                 <td class="left">
                                     <strong class="text-dark">Total</strong> </td>
                                 <td class="right">
                                     <strong class="text-dark">{{$total}}</strong>
                                 </td>
                             </tr>
                         </tbody>
                     </table>
                 </div>
             </div>
         </div>
         <div class="card-footer bg-white">
             <p class="mb-0">singhsolution.com, Nathdwara, Rajsthan, 313301</p>
         </div>
     </div>
 </div>
</main>
@endsection