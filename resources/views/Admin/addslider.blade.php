@extends('layouts.admin')

@section('content')
  <div id="layoutSidenav_content">
        <main> 
            <div class="container-fluid p-4">                   
                 <form action="sliderc" method="post" enctype="multipart/form-data">
                  @csrf
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                          <div class="card border-dark mb-3">
                              <div class="card-header text-white bg-dark">Add Slider</div>
                              <div class="card-body text-dark">
                                 <div class="form-group">
                                    <label for="">Slider 1 Text</label>
                                    <input type="text" name="S_text1" class="form-control" id="" placeholder="">
                                    @error('S_text1')            
                                      <p class="text-danger">{{$message}}</p>
                                    @enderror
                                  </div>                  
                                  <div class="form-group">
                                    <label for="">Slider 1 text Containt</label>
                                    <textarea name="S_content1" class="form-control" id="" rows="3"></textarea>
                                    @error('S_content1')            
                                      <p class="text-danger">{{$message}}</p>
                                    @enderror
                                  </div>
                                  <div class="form-group">
                                    <label for="">Select Slider 1 Image:</label>
                                    <input type="file" name="S_img1" class="form-control-file" id="">
                                    @error('S_img1')            
                                      <p class="text-danger">{{$message}}</p>
                                    @enderror
                                  </div>
                                  <div class="card-header text-white bg-dark">Slider 2</div><br/>
                                  <div class="form-group">
                                    <label for="">Slider 2  Text</label>
                                    <input type="text" name="S_text2" class="form-control" id="" placeholder="">
                                    @error('S_text2')            
                                      <p class="text-danger">{{$message}}</p>
                                    @enderror
                                  </div>                  
                                  <div class="form-group">
                                    <label for="">Slider 2 text Containt</label>
                                    <textarea name="S_content2" class="form-control" id="" rows="3"></textarea>
                                    @error('S_content2')            
                                      <p class="text-danger">{{$message}}</p>
                                    @enderror
                                  </div>
                                  <div class="form-group">
                                    <label for="">Select Slider 2 Image:</label>
                                    <input type="file" name="S_img2" class="form-control-file" id="">
                                    @error('S_img2')            
                                      <p class="text-danger">{{$message}}</p>
                                    @enderror
                                  </div>
                                  <button type="submit" class="btn btn-outline-success">Update</button>
                                  <a class="btn btn-outline-primary" href="http://127.0.0.1:8000/admin/home">Back</a>
                                </div>
                            </div>                            
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </form>
            </div>                  
        </main>                            
@endsection

