@extends('layouts.admin')

@section('content')
  <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">{{$title}}</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="home">Dashboard</a></li>
                            <li class="breadcrumb-item active">{{$title}}</li>
                        </ol> 
                        @if(Session::get('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                              {{Session::get('status')}}
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                        @endif                                             
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                               {{$title}}
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Order By </th>
                                                <th>Phone No</th>
                                                <th>Email</th>
                                                <th>Adress</th>
                                                <th>Delivery Date</th>
                                                <th>Pyment Method</th>
                                                <th>Product Name</th>
                                                <th>Product Model</th>
                                                <th>Product Image</th>
                                                <th>Price</th>
                                                <th>Status</th>
                                                <th>Chang Status</th>                           
                                            </tr>
                                        </thead>                                        
                                        <tbody> 
                                        @foreach($order as $item)
                                            <tr>
                                                <td>{{$item->orderAddress[0]->firstname}} {{$item->orderAddress[0]->lastname}}</td>
                                                <td>{{$item->orderAddress[0]->phon}}</td>
                                                <td>{{$item->orderAddress[0]->email}}</td>
                                                <td><b>Street:</b>{{$item->orderAddress[0]->street}}<br>
                                                    <b>Landmark :</b> {{$item->orderAddress[0]->landmark}}<br>
                                                    <b>City:</b>{{$item->orderAddress[0]->city}}<br>
                                                    <b>Postel Cod:</b>{{$item->orderAddress[0]->zip}}<br/>
                                                    <b>Country:</b>{{$item->orderAddress[0]->country}} 
                                                </td>
                                                <td>{{$item->delivery_date}}</td>
                                                <td>{{$item->payment_id}}</td>
                                                <td>{{$item->product[0]->name}}</td>
                                                <td>{{$item->product[0]->name}}</td>
                                                <td><img src="{{url($item->product[0]->cover_photo)}}" width="100px"></td>
                                                <td>{{$item->gross_price}}</td>
                                                <td>{{$item->status}}</td>
                                                <td id="edit"><a class="btn btn-primary" data-toggle="modal" data-target="#status_model" value=" {{$item->id}}">Edit</a></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>                            

 <!---------------------------------------category-mdel-start------------------------------->
<!---------------------------------------category-mdel-start------------------------------->
<div class="modal fade" id="status_model" tabindex="-1" role="dialog" aria-labelledby="        exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="status-form" >
        <form method="post" action="update/orderstatus">
          @csrf          
          <div class="form-group">
            <label for="">Select Status</label>
            <select name="stetus" class="form-control" id="stetus">
              <option disabled=""selected="">Select</option>
              <option value="processing">processing</option>
              <option value="packed">packed</option>             
              <option value="onway">On Way</option>             
              <option value="delivered">Delivered</option>             
            </select>
            <input type="text" id="order_id" name="order_id" value="" hidden>            
          </div>                                                  
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" id="status-submit" class="btn btn-primary">Update</button>
        </form>
      </div>
      <div class="modal-footer">        
    </div>
  </div>
</div>
<!---------------------------------------category-mdel-end------------------------------->
<!---------------------------------------category-mdel-end------------------------------->
<script>
    $(document).ready(function(){        
        $('#edit a').on('click',function(e){
            var id = $(this).attr('value');
            $('#order_id').empty();
            $('#order_id').val(id);                        
        });
    });
</script>
@endsection