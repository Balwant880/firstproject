@extends('layouts.admin')

@section('content')
<div class="container"> 
    <h1 class="text-center">Admin Login</h1>
@if(Session::get('status'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      {{Session::get('status')}}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
@endif          
      <form action="adminlogin" method="POST">
      @csrf
      <div class="form-group">
        <label>Email</label>
          <input type="email" class="form-control" name="email"  placeholder="Enter Email">        
          @error('email')            
            <p class="text-danger">{{$message}}</p>
          @enderror
      </div>
      <div class="form-group">
        <label>Password</label>
          <input type="password" class="form-control" name="password"  placeholder="Enter Password">        
          @error('password')            
            <p class="text-danger">{{$message}}</p>
          @enderror
      </div>
      @if(!empty($error))
          <div class="alert alert-danger">{{$error}}</div>
      @endif                  
      <br><button type="submit" class="btn btn-success">Login</button>
      <a href="ragister" class="btn btn-primary">SignUp</a>
    </form>
</div>
@endsection
