@extends('layouts.admin')

@section('content')
  <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">{{$title}}</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="home">Dashboard</a></li>
                            <li class="breadcrumb-item active">{{$title}}</li>
                        </ol> 
                         @if(Session::get('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                              {{Session::get('status')}}
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                        @endif                                             
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                               {{$title}}
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Parent subcategory</th>
                                                <th>Stetus</th>
                                                <th>Edit</th>
                                                <th>Delete</th>                                 
                                            </tr>
                                        </thead>                                        
                                        <tbody> 
                                        @foreach($subcategory as $item)
                                            <tr>
                                                <td>{{$item->name}}</td>                        
                                                <td>{{$item->category[0]->name}}</td>
                                                <td>{{$item->active}}</td>
                                                <td id="edit"><a class="btn btn-primary" data-toggle="modal" data-target="#subcategory_model" id="editcat{{$item->id}}" value="{{$item->id}}">Edit</a></td>
                                                <td><a class="btn btn-danger">Delete</a></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>                            
<!---------------------------------------subcategory-mdel-start------------------------------->
<!---------------------------------------subcategory-mdel-start------------------------------->
<div class="modal fade" id="subcategory_model" tabindex="-1" role="dialog" aria-labelledby="        exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit subcategory</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="needs-validation" action="update/subcategory" method="post" enctype="multipart/form-data" novalidate>
          @csrf
          <div class="form-group">            
            <label >Name</label>
             <input type="text" id="name" name="name" class="form-control" placeholder="Enter subcategory Name" value="" required>
             <div class="invalid-feedback">
                  Please enter a valid Name for subcategory
            </div>                           
          <div class="invalid-feedback">
                  Please select image for subcategory
          </div>
          <div class="form-group">
            <label for="">Select Parent Category</label>
            <select name="category" id="p_cat" class="form-control" value="">
              <option disabled=""selected="">Select</option>

            </select>
          </div>          
          <div class="form-group">
            <label for="">Select Status</label>
            <select name="stetus" class="form-control" id="stetus">
              <option disabled=""selected="">Select</option>
              <option value="Y">Yes</option>
              <option value="N">No</option>             
            </select>
          </div>          
            <input type="text" name="id" id="bid" value="" hidden="" required>                            
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </form>
      </div>
      <div class="modal-footer">        
    </div>
  </div>
</div>
<!---------------------------------------subcategory-mdel-end------------------------------->
<!---------------------------------------subcategory-mdel-end------------------------------->                
<script>
    $(document).ready(function(){        
        $('#edit a').on('click',function(e){
            var id = $(this).attr('value');
            $('#bid').val(id);
            $.ajaxSetup({
              headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                     });
            $.ajax({
                url : "http://127.0.0.1:8000/admin/upsubcategory",
                dataType : 'json',
                type : "POST",
                data:{
                       id:id
                     },
                success : function(data){
                    console.log(data);
                    var len = data.subcategory.length;
                    for(var i=0; i<len; i++){
                         var name = data.subcategory[i].name;        
                         var stetus = data.subcategory[i].active;
                         var cat_p = data.subcategory[i].category[i].id;
                         console.log(cat_p);                          
                         var cat_len = data.allcat.length;
                         $('#p_cat').empty();
                         for(var i=0; i<cat_len; i++)
                         {
                           var cat_id = data.allcat[i].id;
                           var cat_name = data.allcat[i].name;                               
                           var option = '<option  value="'+cat_id+'">'+cat_name+'</option>';
                           $('#p_cat').append(option)                           
                         }
                         $('#p_cat').val(cat_p);                             
                         $('#name').val(name);
                         $('#stetus').val(stetus);                         
                   }                                                  
                }
            });                   
        });
    });
</script>
@endsection