@extends('layouts.admin')

@section('content')
  <div id="layoutSidenav_content">
        <main> 
            <div class="container-fluid p-4">                 
                 <form action="add/post" method="post" enctype="multipart/form-data">
                  @csrf
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                          <div class="card border-dark mb-3">                            
                              <div class="card-header text-white bg-dark">Add post</div>
                              <div class="card-body text-dark">
                                @if(Session::get('status'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                  {{Session::get('status')}}
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                 @endif
                                 <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text" name="title" class="form-control" id="" placeholder="Enter A post Title">
                                    @error('title')            
                                      <p class="text-danger">{{$message}}</p>
                                    @enderror
                                  </div>                  
                                  <div class="form-group">
                                    <label for="">Select post Image:</label>
                                    <input type="file" name="post" class="form-control-file" id="">
                                    @error('post')            
                                      <p class="text-danger">{{$message}}</p>
                                    @enderror
                                  </div>                                  
                                  <div class="form-group">
                                    <label for="">Content</label>
                                    <textarea name="content" class="form-control" id="" rows="3"></textarea>
                                    @error('content')            
                                      <p class="text-danger">{{$message}}</p>
                                    @enderror
                                  </div>
                                  <button type="submit" class="btn btn-outline-success">Add</button>
                                  <a class="btn btn-outline-primary" href="http://127.0.0.1:8000/admin/home">Back</a>
                                </div>
                            </div>                            
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </form>
            </div>                  
        </main>                            
@endsection

