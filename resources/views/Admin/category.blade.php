@extends('layouts.admin')

@section('content')
  <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">{{$title}}</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="home">Dashboard</a></li>
                            <li class="breadcrumb-item active">{{$title}}</li>
                        </ol> 
                        @if(Session::get('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                              {{Session::get('status')}}
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                        @endif                                             
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                               {{$title}}
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Image</th>
                                                <th>Stetus</th>
                                                <th>Edit</th>
                                                <th>Delete</th>                                 
                                            </tr>
                                        </thead>                                        
                                        <tbody> 
                                        @foreach($category as $item)
                                            <tr>
                                                <td>{{$item->name}}</td>
                                                <td><img src="{{url($item->image)}}" width="100px"></td>
                                                <td>{{$item->active}}</td>
                                                <td id="edit"><a class="btn btn-primary" data-toggle="modal" data-target="#category_model" id="editcat{{$item->id}}" value="{{$item->id}}">Edit</a></td>
                                                <td><a class="btn btn-danger">Delete</a></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>                            

 <!---------------------------------------category-mdel-start------------------------------->
<!---------------------------------------category-mdel-start------------------------------->
<div class="modal fade" id="category_model" tabindex="-1" role="dialog" aria-labelledby="        exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="needs-validation" action="update/category" method="post" enctype="multipart/form-data" novalidate>
          @csrf
          <div class="form-group">            
            <label >Name</label>
             <input type="text" id="name" name="name" class="form-control" placeholder="Enter category Name" value="" required>
             <div class="invalid-feedback">
                  Please enter a valid Name for category
            </div>                 
          <div class="form-group">
            <label for="message-text" class="col-form-label">Image:</label>
            <div id="img2">               
            </div>
          </div>
          <div id="img" >
              
          </div>
          <div class="invalid-feedback">
                  Please select image for category
          </div>
          <div class="form-group">
            <label for="">Select Status</label>
            <select name="stetus" class="form-control" id="stetus">
              <option disabled=""selected="">Select</option>
              <option value="Y">Yes</option>
              <option value="N">No</option>             
            </select>
          </div>          
            <input type="text" name="id" id="bid" value="" hidden="" required>                            
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </form>
      </div>
      <div class="modal-footer">        
    </div>
  </div>
</div>
<!---------------------------------------category-mdel-end------------------------------->
<!---------------------------------------category-mdel-end------------------------------->
<script>
    $(document).ready(function(){        
        $('#edit a').on('click',function(e){
            var id = $(this).attr('value');
            $('#bid').val(id);
            $.ajaxSetup({
              headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                     });
            $.ajax({
                url : "http://127.0.0.1:8000/admin/upcategory",
                dataType : 'json',
                type : "POST",
                data:{
                       id:id
                     },
                success : function(data){
                    console.log(data.category);
                    var len = data.category.length;
                    for(var i=0; i<len; i++){
                         var name = data.category[i].name;        
                         var stetus = data.category[i].active;                               
                         var img = data.category[i].image;                         
                         $('#name').val(name);
                         $('#stetus').val(stetus);                        
                         $('#img').empty();
                         $('#img').append('<img src ="http://127.0.0.1:8000/'+img+'" width="100px">'); 
                         $('#img2').empty();  
                         $('#img2').append('<input type="file" placeholder="Select Image " class="form-control" name="img" value='+img+'>');
                   }                                                  
                }
            });                   
        });
    });
</script>
@endsection