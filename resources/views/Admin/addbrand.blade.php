@extends('layouts.admin')

@section('content')
  <div id="layoutSidenav_content">
        <main> 
            <div class="container-fluid p-4">                   
                 <form action="add/brand" method="post" enctype="multipart/form-data">
                  @csrf
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                          <div class="card border-dark mb-3">
                              <div class="card-header text-white bg-dark">Add Brand</div>
                              <div class="card-body text-dark">
                                 <div class="form-group">
                                    <label for="">Name</label>
                                    <input type="text" name="b_name" class="form-control" id="" placeholder="Enter A brand name">
                                    @error('b_name')            
                                      <p class="text-danger">{{$message}}</p>
                                    @enderror
                                  </div>                  
                                  <div class="form-group">
                                    <label for="">Select Brand Image:</label>
                                    <input type="file" name="b_img" class="form-control-file" id="">
                                    @error('b_img1')            
                                      <p class="text-danger">{{$message}}</p>
                                    @enderror
                                  </div>                                  
                                  <div class="form-group">
                                    <label for="">Brand Details</label>
                                    <textarea name="b_details" class="form-control" id="" rows="3"></textarea>
                                    @error('b_details')            
                                      <p class="text-danger">{{$message}}</p>
                                    @enderror
                                  </div>
                                  <button type="submit" class="btn btn-outline-success">Add</button>
                                  <a class="btn btn-outline-primary" href="http://127.0.0.1:8000/admin/home">Back</a>
                                </div>
                            </div>                            
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </form>
            </div>                  
        </main>                            
@endsection

