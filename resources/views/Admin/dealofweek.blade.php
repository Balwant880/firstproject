@extends('layouts.admin')

@section('content')
<div id="layoutSidenav_content">
	<main>
		<div class="container-fluid p-4">                   
                 <form action="add/dealofweek" method="post" enctype="multipart/form-data">
                  @csrf
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                          <div class="card border-dark mb-3">
                              <div class="card-header text-white bg-dark">Deal Of The Week</div>
                              <div class="card-body text-dark">
                              	<div class="form-group">
                                    <label for="">Content</label>
                                    <textarea name="d_content" class="form-control" id="" rows="3" required></textarea>
                                    @error('d_content')            
                                      <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Select Product</label>            
                                    <select name="d_product"  id="cat" class="js-example-basic-single" style="width:100%">
                                      <option disabled="" selected>Select</option>              
                                      @if(isset($products))              
                                        @foreach($products as $item)
                                         <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                      @endif  
                                     </select>            
                                  </div>
                                <div class="form-group">
	                                <label for="message-text" class="col-form-label">Baner Of Background</label>
	                               <input name="img" type="file" class="form-control" placeholder="Select Image " required>
	                               </div>
	                               <div class="invalid-feedback">
	                                   Please select image for product
	                                </div> 
	                               <div class="form-group">
	                                 <label for="" class="col-form-label">Select Start Date And Time</label>
                                     <input type="datetime-local" id="start_time" name="start_time" class="form-control">
	                               </div>
	                               <div class="form-group">
	                                 <label for="" class="col-form-label">Select end Date And Time</label>
                                     <input type="datetime-local" id="end_time" name="end_time" class="form-control">
	                               </div>
	                                <button type="submit" class="btn btn-outline-success">Add</button>
                                    <a class="btn btn-outline-primary" href="http://127.0.0.1:8000/admin/home">Back</a>
                               </div>                             
                            <div class="col-md-2"></div>                            
                        </div>                        
                    </div>
               </form>
        </div>             
	</main>
</div>	
@endsection