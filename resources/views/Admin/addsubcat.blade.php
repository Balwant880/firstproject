@extends('layouts.admin')

@section('content')
  <div id="layoutSidenav_content">
        <main> 
            <div class="container-fluid p-4">                   
                 <form action="add/subcat" method="post" enctype="multipart/form-data">
                  @csrf
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                          <div class="card border-dark mb-3">
                              <div class="card-header text-white bg-dark">Add Subcategory</div>
                              <div class="card-body text-dark">
                                 <div class="form-group">
                                    <label for="">Name</label>
                                    <input type="text" name="sub_name" class="form-control" id="" placeholder="Enter A subcategory name">
                                    @error('b_name')            
                                      <p class="text-danger">{{$message}}</p>
                                    @enderror
                                  </div>  
                                  <div class="form-group">
                                    <label for="">Select Parent Category</label>
                                    <select name="parent_cat" class="form-control" id="">
                                      <option disabled=""selected="">Select</option>
                                      @foreach($category as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                      @endforeach
                                      @error('stetus')            
                                        <p class="text-danger">{{$message}}</p>
                                      @enderror
                                    </select>
                                  </div>                
                                  <div class="form-group">
                                    <label for="">Select Status</label>
                                    <select name="stetus" class="form-control" id="">
                                      <option disabled=""selected="">Select</option>
                                      <option value="Y">Yes</option>
                                      <option value="N">No</option>             
                                      @error('stetus')            
                                        <p class="text-danger">{{$message}}</p>
                                      @enderror
                                    </select>
                                  </div>                                                  
                                  <button type="submit" class="btn btn-outline-success">Add</button>
                                  <a class="btn btn-outline-primary" href="http://127.0.0.1:8000/admin/home">Back</a>
                                </div>
                            </div>                            
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </form>
            </div>                  
        </main>                            
@endsection

