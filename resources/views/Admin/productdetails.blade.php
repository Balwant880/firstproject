@extends('layouts.admin')

@section('content')
  <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">{{$title}}</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="home">Dashboard</a></li>
                            <li class="breadcrumb-item active">{{$title}}</li>
                        </ol> 
                        @if(Session::get('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                              {{Session::get('status')}}
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                        @endif                                             
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                               {{$title}}
                               <div  class="d-flex flex-row-reverse">
                                  <a href="{{url('admin/Orders')}}" ><button class="btn btn-primary">Back</button></a>  
                               </div>                               
                            </div>                                                      
                            <div class="card-body">
                              <div class="table-responsive">  
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>category</th>
                                        <th>Description</th>
                                        <th>Subcategory</th>
                                        <th>Brand</th>
                                        <th>Active</th>
                                        <th>Keyword</th>
                                        <th>Info</th>
                                        <th>Image</th>                                        
                                    </tr>
                                </thead>                                
                                <tbody> 
                                 @foreach($products as $item)                                 
                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->category[0]->name}}</td>
                                        <td>L{{$item->description}}</td>
                                        <td>{{$item->subcategory[0]->name}}</td>
                                        <td>{{$item->brand[0]->name}}</td>
                                        <td>{{$item->active}}</td>
                                        <td>{{$item->product_keyword}}</td>
                                        <td>{{$item->additional_info}}</td>
                                        <td><img src="{{url($item->cover_photo)}}" width="100px"></td>                                        
                                    </tr>                                    
                                    @endforeach                                    
                                </tbody>
                            </table>
                        </div>
                    </div>         
                </div>
            </div>            
       </main>                            
@endsection