@extends('layouts.admin')

@section('content')
  <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">{{$title}}</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="home">Dashboard</a></li>
                            <li class="breadcrumb-item active">{{$title}}</li>
                        </ol> 
                        @if(Session::get('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                              {{Session::get('status')}}
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                        @endif                                             
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                               {{$title}}
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>category</th>
                                        <th>Description</th>
                                        <th>Subcategory</th>
                                        <th>Brand</th>
                                        <th>Active</th>
                                        <th>Keyword</th>
                                        <th>Info</th>
                                        <th>Image</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>                                
                                <tbody> 
                                 @foreach($products as $item)                                 
                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->category[0]->name}}</td>
                                        <td>L{{$item->description}}</td>
                                        <td>{{$item->subcategory[0]->name}}</td>
                                        <td>{{$item->brand[0]->name}}</td>
                                        <td>{{$item->active}}</td>
                                        <td>{{$item->product_keyword}}</td>
                                        <td>{{$item->additional_info}}</td>
                                        <td><img src="{{url($item->cover_photo)}}" width="100px"></td>
                                        <td id="edit"><a class="btn btn-primary" data-toggle="modal" data-target="#products_model" id="editcat{{$item->id}}" value="{{$item->id}}">Edit</a></td></a></td>
                                        <td><a class="btn btn-danger text-white" href="#">Delete</a></td>
                                    </tr>
                                    @endforeach                                    
                                </tbody>
                            </table>
                        </div>
                    </div>         
                </div>
            </div>
       </main>                            

 <!---------------------------------------products-mdel-start------------------------------->
<!---------------------------------------products-mdel-start------------------------------->
<div class="modal fade" id="products_model" tabindex="-1" role="dialog" aria-labelledby="        exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="needs-validation" action="update/products" method="post" enctype="multipart/form-data" novalidate>
          @csrf
         <div class="form-group">
            <label >Name</label>
            <input type="text" id="name" name="name" class="form-control" placeholder="Enter product Name" required>
                <div class="invalid-feedback">
                  Please enter a valid Name for product
                </div>                 
          </div>           
          <div class="form-group">
            <label>Select Category</label>            
            <select name="category"  id="cat" class="js-example-basic-single" style="width: 100%">
              <option disabled="">Select</option>
              @if(!empty($category))              
               @foreach($category as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
              @endif  
             </select>            
          </div>
          <div class="form-group">
            <label>Select subcategory</label>
            <select name="subcategory" id="subcat" class="form-control">              
              <option id="subcategory" value="">Select</option>                            
            </select>           
          </div>
          <div class="form-group">
            <label for="">Select Brand</label>
            <select name="brand" class="form-control" id="brand">
              <option disabled=""selected="">Select</option>
              @if(!empty($brand))              
              @foreach($brand as $item)
              <option  value="{{$item->id}}">{{$item->name}}</option>              
              @endforeach
              @endif
            </select>
          </div>
          <div class="form-group">
            <label for="">Select Status</label>
            <select name="stetus" class="form-control" id="stetus">
              <option disabled=""selected="">Select</option>
              <option value="Y">Yes</option>
              <option value="N">No</option>             
            </select>
          </div>
          <div class="form-group">
            <label >Info</label>
            <input name ="info" id="info" type="text" class="form-control" placeholder="Enter Info " required>
                <div class="invalid-feedback">
                  Please enter a valid info for product
                </div>                 
          </div>           
          <div class="form-group">
            <label for="message-text" class="col-form-label">Image:</label>
            <div id="img2">               
            </div>
          </div>
          <div id="img" >
              
          </div>
          <div class="invalid-feedback">
                  Please select image for category
          </div>
          <div class="form-group">
            <label>Description</label>
            <textarea name="description" id="description" class="form-control" required></textarea>
            <div class="invalid-feedback">
                  Please enter a valid description for product
           </div>
          </div>
          <div class="form-group">
            <label>Keywords</label>
            <textarea name="keywords" id="keywords" class="form-control" required></textarea>
            <div class="invalid-feedback">
                  Please enter a valid keywords for product
           </div>
          </div>
          <input type="text" name="id" id="bid" value="" hidden="" required>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!---------------------------------------products-mdel-end------------------------------->
<!---------------------------------------products-mdel-end------------------------------->
<script>
    $(document).ready(function(){        
        $('#edit a').on('click',function(e){
            var id = $(this).attr('value');
            $('#bid').val(id);
            $.ajaxSetup({
              headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                     });
            $.ajax({
                url : "http://127.0.0.1:8000/admin/upproducts",
                dataType : 'json',
                type : "POST",
                data:{
                       id:id
                     },
                success : function(data){
                    console.log(data);
                    var len = data.products.length;                    
                    for(var i=0; i<len; i++){
                         var name = data.products[i].name;        
                         var stetus = data.products[i].active;                         
                         var img = data.products[i].cover_photo;
                         var category = data.products[i].category[i].id;
                         var description = data.products[i].description;
                         var subcatid = data.products[i].subcategory[i].id;
                         var brand = data.products[i].brand[i].id;
                         var keywords = data.products[i].product_keyword;
                         var info = data.products[i].additional_info;        
                         $('#name').val(name);
                         $('#stetus').val(stetus);
                         $('#cat').val(category);
                         $('#description').val(description);
                         $('#keywords').val(keywords);
                         $('#info').val(info);
                         $('#brand').val(brand);                         
                         $('#img').empty();
                         $('#img').append('<img src ="http://127.0.0.1:8000/'+img+'" width="100px">'); 
                         $('#img2').empty();  
                         $('#img2').append('<input type="file" placeholder="Select Image " class="form-control" name="img" value='+img+'>');
                       }               
                       var cat_id = category;             
                        $.ajaxSetup({
                        headers: {
                               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                  }
                           });
                     $.ajax({
                      url:'http://127.0.0.1:8000/admin/subcat',
                      dataType : 'json',
                      type:"POST",                      
                      data: {
                         cat_id:cat_id
                       },
                      success : function(response){
                        console.log(response.subcate);                        
                        var len = response.subcate.length;
                        $('#subcat').empty();
                        for(var i=0; i<len; i++){
                               var name = response.subcate[i].name;        
                               var id = response.subcate[i].id;                               
                               var option = '<option value="'+id+'">'+name+'</option>';
                               $('#subcat').append(option);
                               $('#subcat').val(subcatid);      
                             }                                                  
                        }
                    });                      
                }
            });                   
        });
        $('.js-example-basic-single').on('change',function(e)
            {             
              var cat_id = $( ".js-example-basic-single option:selected" ).val();             
              console.log(cat_id);
              $.ajaxSetup({
                        headers: {
                               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                  }
                           });
               $.ajax({
                      url:'http://127.0.0.1:8000/admin/subcat',
                      dataType : 'json',
                      type:"POST",                      
                      data: {
                         cat_id:cat_id
                       },
                      success : function(response){
                        console.log(response.subcate);                        
                        var len = response.subcate.length;
                        $('#subcat').empty();
                        for(var i=0; i<len; i++){
                               var name = response.subcate[i].name;        
                               var id = response.subcate[i].id;                               
                               var option = '<option value="'+id+'">'+name+'</option>';
                               $('#subcat').append(option);   
                             }                                                  
                          }
                       });                      
                  }); 
    });
</script>
@endsection