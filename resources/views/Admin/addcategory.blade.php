@extends('layouts.admin')

@section('content')
  <div id="layoutSidenav_content">
        <main> 
            <div class="container-fluid p-4">                   
                 <form class="needs-validation" action="add/category" method="post" enctype="multipart/form-data" novalidate>
                  @csrf
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                          <div class="card border-dark mb-3">
                              <div class="card-header text-white bg-dark">Add Slider</div>
                              <div class="card-body text-dark"> 
                                      <div class="form-group">
                                          <label >Name</label>
                                             <input type="text" name="name" class="form-control" placeholder="Enter Category Name" required>
                                               <div class="invalid-feedback">
                                                  Please enter a valid Name for Category
                                               </div>                 
                                              <div class="form-group">
                                                <label for="message-text" class="col-form-label">Image:</label>
                                               <input name="img" type="file" class="form-control" placeholder="Select Image " required>
                                               </div>
                                                <div class="invalid-feedback">
                                                   Please select image for product
                                                </div>                             
                                                <div class="form-group">
                                                  <label for="">Select Status</label>
                                                  <select name="stetus" class="form-control" id="">
                                                    <option disabled=""selected="">Select</option>
                                                   <option value="Y">Yes</option>
                                                    <option value="N">No</option>             
                                                   </select>
                                                </div>          
                                                <button type="submit" class="btn btn-outline-success">Add</button>
                                                <a class="btn btn-outline-primary" href="http://127.0.0.1:8000/admin/home">Back</a>
                                        </div>
                                  </div>                            
                               <div class="col-md-2"></div>
                          </div>
                     </div>
                  </form>
             </div>          
        </main>      
    </div>                      
@endsection