@extends('layouts.admin')

@section('content')
  <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">{{$title}}</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="home">Dashboard</a></li>
                            <li class="breadcrumb-item active">{{$title}}</li>
                        </ol> 
                        @if(Session::get('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                              {{Session::get('status')}}
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                        @endif                                             
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                               {{$title}}
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Post</th>
                                        <th>Title</th>
                                        <th>Content</th>                                        
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>                                
                                <tbody> 
                                 @foreach($post as $item)                                 
                                    <tr>
                                        <td><img src="{{url($item->image)}}" width="100px"></td>
                                        <td>{{$item->caption}}</td>
                                        <td>{{$item->content}}</td>       
                                        <td id="edit"><a class="btn btn-primary" data-toggle="modal" data-target="#products_model" id="editcat{{$item->id}}" value="{{$item->id}}">Edit</a></td></a></td>
                                        <td><a class="btn btn-danger text-white" href="#">Delete</a></td>
                                    </tr>
                                    @endforeach                                    
                                </tbody>
                            </table>
                        </div>
                    </div>         
                </div>
            </div>
       </main>                            
@endsection