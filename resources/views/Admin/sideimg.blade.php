@extends('layouts.admin')

@section('content')
  <div id="layoutSidenav_content">
        <main> 
            <div class="container-fluid p-4">                   
                 <form action="sideimgc" method="post" enctype="multipart/form-data">
                  @csrf
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                          <div class="card border-dark mb-3">
                              <div class="card-header text-white bg-dark">Add Sidebar Images</div>
                              <div class="card-body text-dark">
                                 <div class="form-group">
                                    <label for="">Left Sidebar Image</label>
                                    <input type="file" name="l_img" class="form-control" id="">
                                    @error('l_img')            
                                      <p class="text-danger">{{$message}}</p>
                                    @enderror
                                  </div>                  
                                  <div class="form-group">
                                    <label for="">Right Sidebar Image</label>
                                    <input type="file" name="r_img" class="form-control" id="">
                                    @error('r_img')            
                                      <p class="text-danger">{{$message}}</p>
                                    @enderror
                                  </div>                                
                                  <button type="submit" class="btn btn-outline-success">Update</button>
                                  <a class="btn btn-outline-primary" href="http://127.0.0.1:8000/admin/home">Back</a>
                                </div>
                            </div>                            
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </form>
            </div>                  
        </main>                            
@endsection

