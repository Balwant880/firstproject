@extends('layouts.admin')

@section('content')
  <div id="layoutSidenav_content">
        <main> 
              <div class="container-fluid p-4">                   
                 <form class="needs-validation" action="add/product" method="post" enctype="multipart/form-data" novalidate>
                  @csrf
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                          <div class="card border-dark mb-3">
                              <div class="card-header text-white bg-dark">Add Product</div>
                              <div class="card-body text-dark">
               
                                 <div class="form-group">
                                    <label >Name</label>
                                    <input type="text" name="name" class="form-control" placeholder="Enter product Name" required>
                                        <div class="invalid-feedback">
                                          Please enter a valid Name for product
                                        </div>                 
                                  </div>           
                                  <div class="form-group">
                                    <label>Select Category</label>            
                                    <select name="category"  id="cat" class="js-example-basic-single" style="width:100%">
                                      <option disabled="" selected>Select</option>              
                                      @if(isset($category))              
                                        @foreach($category as $item)
                                         <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                      @endif  
                                     </select>            
                                  </div>
                                  <div class="form-group">
                                    <label>Select subcategory</label>
                                    <select name="subcategory" id="eachsubcat" class="form-control">              
                                      <option id="subcategory" value="">Select</option>                            
                                    </select>           
                                  </div>
                                  <div class="form-group">
                                    <label for="">Select Brand</label>
                                    <select name="brand" class="form-control" id="">
                                      <option disabled=""selected="">Select</option>
                                      @if(!empty($brand))              
                                      @foreach($brand as $item)
                                      <option  value="{{$item->id}}">{{$item->name}}</option>
                                      @endforeach
                                      @endif
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="">Select Status</label>
                                    <select name="stetus" class="form-control" id="">
                                      <option disabled=""selected="">Select</option>
                                      <option value="Y">Yes</option>
                                      <option value="N">No</option>             
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label >Info</label>
                                    <input name ="info" type="text" class="form-control" placeholder="Enter Info " required>
                                        <div class="invalid-feedback">
                                          Please enter a valid info for product
                                        </div>                 
                                  </div>           
                                  <div class="form-group">
                                    <label>Cover_photo</label>
                                    <input name="img" type="file" class="form-control" placeholder="Select Image " required>
                                    <div class="invalid-feedback">
                                          Please select image for product
                                        </div>                             
                                  </div>
                                  <div class="form-group">
                                    <label>Select Images</label>
                                    <input class="btn btn-primary form-control"  type="file" name="files[]" multiple>
                                    <div class="invalid-feedback">
                                          Please select image for product
                                        </div>                             
                                  </div>
                                  <div class="form-group">
                                    <label>Description</label>
                                    <textarea name="description" class="form-control" required></textarea>
                                    <div class="invalid-feedback">
                                          Please enter a valid description for product
                                   </div>
                                  </div>
                                  <div class="form-group">
                                    <label>Keywords</label>
                                    <textarea name="keywords" class="form-control" required></textarea>
                                    <div class="invalid-feedback">
                                          Please enter a valid keywords for product
                                   </div>
                                  </div>
                                  <div class="form-group">
                                    <label >Price</label>
                                    <input type="text" name="price" class="form-control" placeholder="Enter product Price" required>
                                        <div class="invalid-feedback">
                                          Please enter a valid Price for product
                                        </div>                 
                                  </div>           
                                  <button type="submit" class="btn btn-outline-success">Add</button>
                                  <a class="btn btn-outline-primary" href="http://127.0.0.1:8000/admin/home">Back</a>
                                   </div>
                                    </div>                            
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </form>
            </div>          
        </main>                            
@endsection

