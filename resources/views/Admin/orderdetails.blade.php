@extends('layouts.admin')

@section('content')
<style>
    body {
        color: #566787;
		background: #f5f5f5;
		font-family: 'Varela Round', sans-serif;
		font-size: 13px;
	}
	.table-wrapper {
        background: #fff;
        padding: 20px 25px;
        margin: 30px auto;
		border-radius: 3px;
        box-shadow: 0 1px 1px rgba(0,0,0,.05);
    }
	.table-wrapper .btn {
		float: right;
		color: #333;
    	background-color: #fff;
		border-radius: 3px;
		border: none;
		outline: none !important;
		margin-left: 10px;
	}
	.table-wrapper .btn:hover {
        color: #333;
		background: #f2f2f2;
	}
	.table-wrapper .btn.btn-primary {
		color: #fff;
		background: #03A9F4;
	}
	.table-wrapper .btn.btn-primary:hover {
		background: #03a3e7;
	}
	.table-title .btn {		
		font-size: 13px;
		border: none;
	}
	.table-title .btn i {
		float: left;
		font-size: 21px;
		margin-right: 5px;
	}
	.table-title .btn span {
		float: left;
		margin-top: 2px;
	}
	.table-title {
		color: #fff;
		background: #4b5366;		
		padding: 16px 25px;
		margin: -20px -25px 10px;
		border-radius: 3px 3px 0 0;
    }
    .table-title h2 {
		margin: 5px 0 0;
		font-size: 24px;
	}
	.show-entries select.form-control {        
        width: 60px;
		margin: 0 5px;
	}
	.table-filter .filter-group {
        float: right;
		margin-left: 15px;
    }
	.table-filter input, .table-filter select {
		height: 34px;
		border-radius: 3px;
		border-color: #ddd;
        box-shadow: none;
	}
	.table-filter {
		padding: 5px 0 15px;
		border-bottom: 1px solid #e9e9e9;
		margin-bottom: 5px;
	}
	.table-filter .btn {
		height: 34px;
	}
	.table-filter label {
		font-weight: normal;
		margin-left: 10px;
	}
	.table-filter select, .table-filter input {
		display: inline-block;
		margin-left: 5px;
	}
	.table-filter input {
		width: 200px;
		display: inline-block;
	}
	.filter-group select.form-control {
		width: 110px;
	}
	.filter-icon {
		float: right;
		margin-top: 7px;
	}
	.filter-icon i {
		font-size: 18px;
		opacity: 0.7;
	}	
    table.table tr th, table.table tr td {
        border-color: #e9e9e9;
		padding: 12px 15px;
		vertical-align: middle;
    }
	table.table tr th:first-child {
		width: 60px;
	}
	table.table tr th:last-child {
		width: 80px;
	}
    table.table-striped tbody tr:nth-of-type(odd) {
    	background-color: #fcfcfc;
	}
	table.table-striped.table-hover tbody tr:hover {
		background: #f5f5f5;
	}
    table.table th i {
        font-size: 13px;
        margin: 0 5px;
        cursor: pointer;
    }	
	table.table td a {
		font-weight: bold;
		color: #566787;
		display: inline-block;
		text-decoration: none;
	}
	table.table td a:hover {
		color: #2196F3;
	}
	table.table td a.view {        
		width: 30px;
		height: 30px;
		color: #2196F3;
		border: 2px solid;
		border-radius: 30px;
		text-align: center;
    }
    table.table td a.view i {
        font-size: 22px;
		margin: 2px 0 0 1px;
    }   
	table.table .avatar {
		border-radius: 50%;
		vertical-align: middle;
		margin-right: 10px;
	}
	.status {
		font-size: 30px;
		margin: 2px 2px 0 0;
		display: inline-block;
		vertical-align: middle;
		line-height: 10px;
	}
    .text-success {
        color: #10c469;
    }
    .text-info {
        color: #62c9e8;
    }
    .text-warning {
        color: #FFC107;
    }
    .text-danger {
        color: #ff5b5b;
    }
    .pagination {
        float: right;
        margin: 0 0 5px;
    }
    .pagination li a {
        border: none;
        font-size: 13px;
        min-width: 30px;
        min-height: 30px;
        color: #999;
        margin: 0 2px;
        line-height: 30px;
        border-radius: 2px !important;
        text-align: center;
        padding: 0 6px;
    }
    .pagination li a:hover {
        color: #666;
    }	
    .pagination li.active a {
        background: #03A9F4;
    }
    .pagination li.active a:hover {        
        background: #0397d6;
    }
	.pagination li.disabled i {
        color: #ccc;
    }
    .pagination li i {
        font-size: 16px;
        padding-top: 6px
    }
    .hint-text {
        float: left;
        margin-top: 10px;
        font-size: 13px;
    }    
</style>
<script>
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
});
</script>
</head>
<body>
<div id="layoutSidenav_content">
 <main>
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-4">
						<h2>Order <b>Details</b></h2>
					</div>
					<div class="col-sm-8">						
						<a href="#" class="btn btn-primary"><i class="material-icons">&#xE863;</i> <span>Refresh List</span></a>
						<a href="#" class="btn btn-info"><i class="material-icons">&#xE24D;</i> <span>Export to Excel</span></a>
					</div>
                </div>
            </div>			
            @if(Session::get('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                              {{Session::get('status')}}
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                         </div>
            @endif                                             
           <div class="card-body">
                                <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
				                    <tr>				                        
				                        <th>Customer</th>
										<th>Location</th>
										<th>Delivery Date</th>
										<th>Payment</th>						
				                        <th>Status</th>						
										<th>Net Amount</th>
										<th>Action</th>
										<th>product Detail</th>
				                    </tr>
				                </thead>
                                <tbody>
                                @foreach($order as $item)
                                            <tr>
                                                <td>{{$item->orderAddress[0]->firstname}} {{$item->orderAddress[0]->lastname}}</td>
                                                <td><b>Street:</b>{{$item->orderAddress[0]->street}}<br>
                                                    <b>Landmark :</b> {{$item->orderAddress[0]->landmark}}<br>
                                                    <b>City:</b>{{$item->orderAddress[0]->city}}<br>
                                                    <b>Postel Cod:</b>{{$item->orderAddress[0]->zip}}<br/>
                                                    <b>Country:</b>{{$item->orderAddress[0]->country}} 
                                                </td>
                                                <td>{{$item->delivery_date}}</td>
                                                <td>{{$item->payment_id}}</td>
                                                <td>{{$item->status}}</td>
                                                <td>{{$item->gross_price}}</td>
                                                <td id="edit"><a class="btn btn-primary" data-toggle="modal" data-target="#status_model" value=" {{$item->id}}">Status</a></td>
                                                <td id="edit"><a class="btn btn-primary" href="{{url('admin/productdetails')}}/{{$item->product[0]->id}}">Detail</a></td>
                                            </tr>
                                    @endforeach
                              </tbody>
                         </table>
                 </div>
           </div>         
    </div> 
  </main>    
</div>
</body>   

<div class="modal fade" id="status_model" tabindex="-1" role="dialog" aria-labelledby="        exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="status-form" >
        <form method="post" action="update/orderstatus">
          @csrf          
          <div class="form-group">
            <label for="">Select Status</label>
            <select name="stetus" class="form-control" id="stetus">
              <option disabled=""selected="">Select</option>
              <option value="processing">processing</option>
              <option value="packed">packed</option>             
              <option value="onway">On Way</option>             
              <option value="delivered">Delivered</option>             
            </select>
            <input type="text" id="order_id" name="order_id" value="" hidden>            
          </div>                                                  
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" id="status-submit" class="btn btn-primary">Update</button>
        </form>
      </div>
      <div class="modal-footer">        
    </div>
  </div>
</div>
<!---------------------------------------category-mdel-end------------------------------->
<!---------------------------------------category-mdel-end------------------------------->
<script>
    $(document).ready(function(){        
        $('#edit a').on('click',function(e){
            var id = $(this).attr('value');
            $('#order_id').empty();
            $('#order_id').val(id);                        
        });
    });
</script>                         	
@endsection