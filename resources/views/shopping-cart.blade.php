@extends('layouts.app')

@section('content')
<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>
    <!-- Breadcrumb Section Begin -->
    <div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text product-more">
                        <a href="./home.html"><i class="fa fa-home"></i> Home</a>
                        <a href="./shop.html">Shop</a>
                        <span>Shopping Cart</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Section Begin -->

    <!-- Shopping Cart Section Begin -->
    <section class="shopping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cart-table">
                        <table>
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th class="p-name">Product Name</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th>Update</th>
                                    <th><i class="ti-close"></i></th>
                                </tr>
                            </thead>
                            <tbody>                                
                                @foreach($cart as $cart)                                
                                <tr id="up_cart">
                                    <form>
                                    @csrf
                                    <td class="cart-pic first-row"><img src="{{url($cart->product[0]->cover_photo)}}" alt="" width="200px"></td>
                                    <td class="cart-title first-row">
                                        <h5>{{$cart->product_name}}</h5>
                                    </td>
                                    <td class="p-price first-row">{{$cart->price/$cart->qty}}</td>
                                    <td class="qua-col first-row">
                                        <div class="quantity">
                                            <div class="pro-qty">
                                                <input type="text" id="up_qty" name="up_qty" value="{{$cart->qty}}">
                                            </div>
                                        </div>
                                    </td>                                    
                                    <td class="total-price first-row">{{$cart->price}}</td>
                                    <td class="p-price first-row">
                                       <button type="submit" class="btn btn-default"><i class="fa fa-refresh" style="font-size:20px;color:blue"></i></button>
                                       <input type="text" value="{{$cart->id}}" id="up_id" name="up_id" hidden/>
                                    </td>
                                    <td class="close-td first-row" id="closeqty"><i class="fa fa-trash" style="font-size:20px;color:red" value="{{$cart->id}}"></i></td>
                                    </form>
                                </tr>                                
                                @endforeach                                                                
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="cart-buttons">
                                <a href="{{url('shop')}}" class="btn btn-success">Continue shopping</a>
                            </div>
                            <div class="discount-coupon">
                                <h6>Discount Codes</h6>
                                <form action="#" class="coupon-form">
                                    <input type="text" placeholder="Enter your codes">
                                    <button type="submit" class="site-btn coupon-btn">Apply</button>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-4 offset-lg-4">
                            <div class="proceed-checkout">
                                <ul>                                    
                                  <li class="cart-total">Total<span>Rs:{{$total}}</span></li>
                                </ul>
                                <a href="{{url('pay')}}" class="proceed-btn">PROCEED TO CHECK OUT</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shopping Cart Section End -->
</body>
@endsection