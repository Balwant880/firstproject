<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">        
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
        
    <meta name="csrf-token" content="{{ csrf_token() }}">    

    <title>Password Confirm</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('ucss/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('ucss/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('ucss/jquery-ui.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('ucss/style.css')}}" type="text/css">
</head>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Confirm Password') }}</div>

                <div class="card-body">
                    {{ __('Please confirm your password before continuing.') }}

                    <form method="POST" action="{{ route('password.confirm') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Confirm Password') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('ujs/jquery-3.3.1.min.js')}}"></script>
<script src="{{ asset('ujs/bootstrap.min.js')}}"></script>