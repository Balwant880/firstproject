<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title></title>
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet">    
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>        
</head>
    <body class="sb-nav-fixed">      
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="index">Start Bootstrap</a>
            <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
               <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      @if(auth()->user()->unreadNotifications->count() > 0)
                      <span class="badge badge-danger">                   
                          {{auth()->user()->unreadNotifications->count()}}
                      </span>
                      @endif
                      <i class="fa fa-bell"></i>
                    </a>
                    @if(auth()->user()->unreadNotifications->count() > 0)
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                      @foreach(auth()->user()->unreadNotifications as $unread)
                         <a class="dropdown-item" href="#">{{ $unread->data['data'] }}</a>
                         <div class="dropdown-divider"></div>
                      @endforeach
                    </div>
                    @endif
               </li>
                <li class="nav-item dropdown m-1">                  
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="profile">{{auth()->user()->name}}</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="setting">Settings</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="logout">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">SinghSolution</div>
                            <a class="nav-link" href="{{url('admin/home')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a>
                            <div class="sb-sidenav-menu-heading">Managment</div>
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                Add Products
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="{{url('admin/addproduct')}}">
                                    Add Product                                    
                                  </a>
                                    <a class="nav-link" href="{{url('admin/addcategory')}}">
                                    Add Categoreis
                                  </a>
                                    <a class="nav-link" href="{{url('admin/addbrand')}}">
                                    Add Brand
                                  </a>
                                  </a>
                                    <a class="nav-link" href="{{url('admin/addsubcat')}}">
                                    Add Subcategory
                                  </a>
                                </nav>
                            </div>
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                Show Products
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                                    <a class="nav-link" href="{{url('admin/products')}}">
                                    Show Products
                                    </a>
                                    <a class="nav-link" href="{{url('admin/post')}}">
                                    Show Post 
                                    </a>
                                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                                        Category
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                    </a>                                    
                                    <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                        <nav class="sb-sidenav-menu-nested nav">
                                            <a class="nav-link" href="{{url('admin/category')}}">Category</a>
                                            <a class="nav-link" href="{{url('admin/subcategory')}}">Subcategory</a>
                                        </nav>
                                    </div>
                                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">
                                        Brand Managment
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                    </a>
                                    <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                        <nav class="sb-sidenav-menu-nested nav">
                                            <a class="nav-link" href="{{url('admin/brand')}}">Brands</a> 
                                        </nav>
                                    </div>                                  
                                </nav>                                
                            </div>
                            <a class="nav-link" href="{{url('admin/Orders')}}">
                              <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                All Orders 
                            </a>
                            <div class="sb-sidenav-menu-heading">Design Managment</div>
                            <a class="nav-link" href="{{url('admin/add/slider')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                Add Slider Banner
                            </a>                           
                            <a class="nav-link" href="{{url('admin/add/sideimg')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Add Sidebar Image
                            </a>
                            <a class="nav-link" href="{{url('admin/dealofweek')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Deal Of the Weak 
                            </a>
                            <a class="nav-link" href="{{url('admin/addpost')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                               Add New Post
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        Start Bootstrap
                    </div>
                </nav>
            </div>
  @yield('content') 
</div>    
<!---------------------------------------category-mdel-start------------------------------->
<!---------------------------------------category-mdel-start------------------------------->
<div class="modal fade" id="category_model" tabindex="-1" role="dialog" aria-labelledby="        exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="needs-validation" action="add/category" method="post" enctype="multipart/form-data" novalidate>
          @csrf
          <div class="form-group">
            <label >Name</label>
             <input type="text" name="name" class="form-control" placeholder="Enter Category Name" required>
             <div class="invalid-feedback">
                  Please enter a valid Name for Category
            </div>                 
          <div class="form-group">
            <label for="message-text" class="col-form-label">Image:</label>
            <input name="img" type="file" class="form-control" placeholder="Select Image " required>
          </div>
          <div class="invalid-feedback">
                  Please select image for product
          </div>                             
          <div class="form-group">
            <label for="">Select Status</label>
            <select name="stetus" class="form-control" id="">
              <option disabled=""selected="">Select</option>
              <option value="Y">Yes</option>
              <option value="N">No</option>             
            </select>
          </div>          
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Add</button>
        </form>
      </div>
      <div class="modal-footer">        
    </div>
  </div>
</div>
<!---------------------------------------category-mdel-end------------------------------->
<!---------------------------------------category-mdel-end------------------------------->

<script src="{{ asset('js/scripts.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script src="{{ asset('js/chart-area-demo.js')}}"></script>
<script src="{{ asset('js/chart-bar-demo.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="{{ asset('js/datatables-demo.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
</body>
</html>
<script>
    (function() {
    'use strict';
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })(); 
</script>  
<script>
  // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2({
              width: 'resolve' 
      });
});
$('.js-example-basic-single').on('change',function(e)
            {             
              var cat_id = $( ".js-example-basic-single option:selected" ).val();             
              console.log(cat_id);
              $.ajaxSetup({
                        headers: {
                               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                  }
                           });
               $.ajax({
                      url:'http://127.0.0.1:8000/admin/subcat',
                      dataType : 'json',
                      type:"POST",                      
                      data: {
                         cat_id:cat_id
                       },
                      success : function(response){
                        console.log(response.subcate);                        
                        var len = response.subcate.length;
                        $('#eachsubcat').empty();
                        for(var i=0; i<len; i++){
                               var name = response.subcate[i].name;        
                               var id = response.subcate[i].id;                               
                               var option = '<option value="'+id+'">'+name+'</option>';
                               $('#eachsubcat').append(option);   
                             }                                                  
                          }
                       });                      
                  }); 
</script>
