<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">        
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
        
    <meta name="csrf-token" content="{{ csrf_token() }}">    

    <title>SinghSolution</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>

    <!-- Css Styles -->    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('ucss/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('ucss/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('ucss/themify-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('ucss/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('ucss/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('ucss/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('ucss/jquery-ui.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('ucss/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('ucss/style.css')}}" type="text/css">        
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> 
    <script src="https://unpkg.com/@popperjs/core@2"></script>  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>      
</head> 
<header>
    <div id="app">        
        <!-- Header Section Begin -->
    <header class="header-section">
        <div class="header-top">
            <div class="container">
                <div class="ht-left">
                    <div class="mail-service">
                        <i class=" fa fa-envelope"></i>
                        hello.colorlib@gmail.com
                    </div>
                    <div class="phone-service">
                        <i class=" fa fa-phone"></i>
                        +65 11.188.888
                    </div>
                </div>                
                        <!-- Authentication Links -->                            
                        
            <div class="ht-right">                                    
                <div class="lan-selector">                               
                @guest                        

                    <a class="m-2 text-dark" href="{{ route('login') }}"><i class="fa fa-user"></i> <b>{{__('Login') }}</b></a>                                              

                    @else                        
                            <span class="nav-item dropdown">       
                            <span class="dropdown-menu dropdown-menu-right"            aria-labelledby="navbarDropdown">
                                <a class="fa fa-sign-out dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                            <a class="fa fa-user dropdown-item" href="{{ url('myorder') }}">
                                    {{ __('Profile') }}
                                </a>                            
                                <form id="logout-form" action="{{ route('logout') }}"      method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </span>
                        </span> 
                        <a id="navbarDropdown" class="text-dark m-2 dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <b class="fa fa-user"> {{ Auth::user()->name }}</b>
                            </a> 
                @endguest                                             
                </div>
                <div class="top-social">
                    <a href="#"><i class="ti-facebook"></i></a>
                    <a href="#"><i class="ti-twitter-alt"></i></a>
                    <a href="#"><i class="ti-linkedin"></i></a>
                    <a href="#"><i class="ti-pinterest"></i></a>
                    
                </div>
            </div>                
        </div>
    </div>
        <div class="container">
            <div class="inner-header">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        <div class="logo">
                            <a class="text-dark navbar-brand" href="{{ url('/') }}">
                                  <b>SinghSolution</b>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <div class="advanced-search">
                            <button type="button" class="category-btn">All Categories</button>
                            <form action="#" class="input-group">
                                <input type="text" placeholder="What do you need?">
                                <button type="button"><i class="ti-search"></i></button>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-3 text-right col-md-3">
                        <ul class="nav-right">
                            <li class="heart-icon">
                                <a href="#">
                                    <i class="icon_heart_alt"></i>
                                    <span>1</span>
                                </a>
                            </li>
                            <li class="cart-icon">
                                <a href="#">
                                    <i class="icon_bag_alt"></i>
                                    <div id="cart_item"></div>
                                </a>
                                <div class="cart-hover">
                                    <div class="select-items">
                                        <table>
                                            <tbody id="cartdata">                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="select-total" id="cart_total">
                                        
                                    </div>
                                    <div class="select-button">
                                        <a href="{{url('/viewcart') }}" class="primary-btn view-card">VIEW CART</a>
                                        <a href="{{url('/checkout') }}" class="primary-btn checkout-btn">CHECK OUT</a>
                                    </div>
                                </div>
                            </li>
                            <li class="cart-price">$150.00</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-item">
            <div class="container">
                <div class="nav-depart">
                    <div class="depart-btn">
                        <i class="ti-menu"></i>
                        <span>All departments</span>
                        <ul class="depart-hover">
                            <li class="active"><a href="#">Women’s Clothing</a></li>
                            <li><a href="#">Men’s Clothing</a></li>
                            <li><a href="#">Underwear</a></li>
                            <li><a href="#">Kid's Clothing</a></li>
                            <li><a href="#">Brand Fashion</a></li>
                            <li><a href="#">Accessories/Shoes</a></li>
                            <li><a href="#">Luxury Brands</a></li>
                            <li><a href="#">Brand Outdoor Apparel</a></li>
                        </ul>
                    </div>                    
                </div>
                <nav class="nav-menu mobile-menu">
                    <ul>
                        <li class="active"><a href="{{url('/')}}">Home</a></li>
                        <li><a href="{{url('/shop')}}">Shop</a></li>
                        <li><a href="#">Collection</a>
                            <ul class="dropdown">
                                <li><a href="#">Men's</a></li>
                                <li><a href="#">Women's</a></li>
                                <li><a href="#">Kid's</a></li>
                            </ul>
                        </li>
                        <li><a href="./blog">Blog</a></li>
                        <li><a href="./contact">Contact</a></li>
                        <li><a href="#">Pages</a>
                            <ul class="dropdown">
                                <li><a href="./blog-details">Blog Details</a></li>
                                <li><a href="./shopping-cart">Shopping Cart</a></li>
                                <li><a href="./check-out">Checkout</a></li>
                                <li><a href="./faq">Faq</a></li>
                                <li><a href="./register">Register</a></li>
                                <li><a href="./login">Login</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <div id="mobile-menu-wrap"></div>
            </div>
        </div>
    </header>
</div>
</header>  
        <main class="py-1">
             @yield('content')
        </main>       
    <!-- Partner Logo Section Begin -->
    <div class="partner-logo">
        <div class="container">
            <div class="logo-carousel owl-carousel">
                <div class="logo-item">
                    <div class="tablecell-inner">
                        <img src="{{ asset('images/img/logo-carousel/logo-1.png')}}" alt="">
                    </div>
                </div>
                <div class="logo-item">
                    <div class="tablecell-inner">
                        <img src="{{ asset('images/img/logo-carousel/logo-2.png')}}" alt="">
                    </div>
                </div>
                <div class="logo-item">
                    <div class="tablecell-inner">
                        <img src="{{ asset('images/img/logo-carousel/logo-3.png')}}" alt="">
                    </div>
                </div>
                <div class="logo-item">
                    <div class="tablecell-inner">
                        <img src="{{ asset('images/img/logo-carousel/logo-4.png')}}" alt="">
                    </div>
                </div>
                <div class="logo-item">
                    <div class="tablecell-inner">
                        <img src="{{ asset('images/img/logo-carousel/logo-5.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Partner Logo Section End -->
        <!-- Footer Section Begin -->
    <footer class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer-left">
                        <div class="footer-logo">
                            <a href="#"><img src="{{ asset('images/img/footer-logo.png')}}" alt=""></a>
                        </div>
                        <ul>
                            <li>Address: 60-49 Road 11378 New York</li>
                            <li>Phone: +65 11.188.888</li>
                            <li>Email: hello.colorlib@gmail.com</li>
                        </ul>
                        <div class="footer-social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 offset-lg-1">
                    <div class="footer-widget">
                        <h5>Information</h5>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Checkout</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Serivius</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="footer-widget">
                        <h5>My Account</h5>
                        <ul>
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Shopping Cart</a></li>
                            <li><a href="#">Shop</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="newslatter-item">
                        <h5>Join Our Newsletter Now</h5>
                        <p>Get E-mail updates about our latest shop and special offers.</p>
                        <form action="#" class="subscribe-form">
                            <input type="text" placeholder="Enter Your Mail">
                            <button type="button">Subscribe</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-reserved">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copyright-text">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </div>
                        <div class="payment-pic">
                            <img src="{{ asset('images/img/payment-method.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="{{ asset('ujs/jquery-3.3.1.min.js')}}"></script>
    <script src="{{ asset('ujs/bootstrap.min.js')}}"></script>
    <script src="{{ asset('ujs/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('ujs/jquery.countdown.min.js')}}"></script>
    <script src="{{ asset('ujs/jquery.nice-select.min.js')}}"></script>
    <script src="{{ asset('ujs/jquery.zoom.min.js')}}"></script>
    <script src="{{ asset('ujs/jquery.dd.min.js')}}"></script>
    <script src="{{ asset('ujs/jquery.slicknav.js')}}"></script>
    <script src="{{ asset('ujs/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('ujs/main.js')}}"></script>
    <script>
    $(document).ready(function(){
        //swal("Welcome!", "Welcome to the site!", "success");
        $("#cartform").submit(function(e) {        
              e.preventDefault();
              var form = $(this);
              var url = form.attr('action');
               $.ajax({
               type: "POST",
               url: url,
               data: form.serialize(),
               success: function(data)
               {
                 if(data.error)
                 {
                     var error = data.error;    
                     $('#error').empty();                
                     var option = ' <p class="alert alert-danger">'+ error +'</p>';
                     $('#error').append(option).fadeIn(1000);
                     $('#error').fadeOut(4000);
                 }
                 else
                 {                    
                    window.location.href='http://127.0.0.1:8000/viewcart';
                 }
               }
          });
    });
      function loadcart(){
      $.ajax({
        url : "http://127.0.0.1:8000/cart-load",
        type : "GET",
        success : function(data){            
            var len = data.cart.length;
            console.log(data)
            $('#cartdata').empty();
            for(var i=0; i<len; i++){
                   var name = data.cart[i].product_name;        
                   var id = data.cart[i].id;                               
                   var qty = data.cart[i].qty;
                   var price = data.cart[i].price;
                   var image = data.cart[i].product[0].cover_photo;                   
                   var cartdata = '<tr><td class="si-pic"><img src="http://127.0.0.1:8000/'+image+'" alt="" width="100px"></td><td class="si-text"><div class="product-selected"><p>'+price+'</p><h6>'+name+'</h6></div></td></tr>';                   
                       $('#cartdata').append(cartdata);                                       
                 }
                 var count = data.cart.length + qty -1;                                  
                 var total = data.total;
                 var cart_total = '<span>total:</span><h5>Rs:'+total+'</h5>'
                 $('#cart_total').append(cart_total);                  
                 $('#cart_item').append('<span>'+count+'</span>');
        }
      });
    }
    loadcart();
    $('#closeqty i').on('click',function(e){
       var  id = $(this).attr('value');       
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this imaginary file!",
          icon: "warning",
          buttons:true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {         
            $.ajaxSetup({
            headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
              });
            $.ajax({
              url:'http://127.0.0.1:8000/deletecart',
              dataType : 'json',
              type:"POST",                      
              data: {
                 id:id
               },
               success : function(data){
                    console.log(data);
                  }
               });                      
              swal(id + "Poof! Your imaginary file has been deleted!", {
              icon: "success",
            });
            location.reload();
          }
        });
    })  
    $.ajaxSetup({
            headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
    });
    $("#up_cart form").submit(function(e) {        
              e.preventDefault();
               var form = $(this);                             
               $.ajax({
               type: "POST",
               url: 'http://127.0.0.1:8000/updatecart',
               data: form.serialize(),
               success: function(data)
               {
                 console.log(data);
                 location.reload();
               }               
        });
    });
    // $('#hometab li a').on('click',function(){
    //     var name = $(this).attr('value');
    //     $.ajax({
    //            type: "GET",
    //            url: 'http://127.0.0.1:8000/gettab/'+name,               
    //            success: function(data)
    //            {                                                    
    //              var len = data.length; 
    //              console.log(data)
    //              $('#product-item').empty();
    //              for(var i=0; i<len; i++){
    //                var name = data[i].name;        
    //                var id = data[i].id;                                                  
    //                var price = data[i].price;
    //                var image = data[i].cover_photo;                                      
    //                // var product = '<div class="product-item" id="productitem"><div class="pi-pic"><img src="http://127.0.0.1:8000/'+image+'"alt=""><div class="sale">Sale</div><div class="icon"><i class="icon_heart_alt"></i></div><ul><li class="w-icon active"><a href="#"><i class="icon_bag_alt"></i></a></li><li class="quick-view"><a href="product/'+id+'">Shop Now</a></li><li class="w-icon"><a href="#"><i class="fa fa-random"></i></a></li></ul></div><div class="pi-text"><div class="catagory-name">Coat</div><a href="#"><h5>'+name+'</h5></a><div class="product-price">$14.00<span>$35.00</span></div></div></div>';
    //               $('#product-name').html('<h5>'+name+'</h5>');                        
    //              }                                   
    //            }               
    //     });        
    // });
    $(function() {
    $('[data-toggle="tooltip"]').tooltip()
    });     
});   
</script>

