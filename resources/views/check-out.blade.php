@extends('layouts.app')

@section('content')
<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>
    <!-- Breadcrumb Section Begin -->
    <div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text product-more">
                        <a href="./index.html"><i class="fa fa-home"></i> Home</a>
                        <a href="./shop.html">Shop</a>
                        <span>Check Out</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Section Begin -->

    <!-- Shopping Cart Section Begin -->
<!--     <form id="rzp-footer-form" action="{!!route('dopayment')!!}" method="POST" style="width: 100%; text-align: center" >
                @csrf
 -->
                <!-- <a href="https://amzn.to/2RlZQXk">
                    <img src="https://images-na.ssl-images-amazon.com/images/I/31tPpWGQWzL.jpg" />
                </a>     -->
<!--                 <br/>
                <p><br/>Price: 2,475 INR </p>
                <input type="hidden" name="amount" id="amount" value="2475"/>
                <div class="pay">
                    <button class="razorpay-payment-button btn filled small" id="paybtn" type="button">Pay with Razorpay</button>                        
                </div>
            </form>
            <br/><br/>
            <div id="paymentDetail" style="display: none">
                <center>
                    <div>paymentID: <span id="paymentID"></span></div>
                    <div>paymentDate: <span id="paymentDate"></span></div>
                </center>
            </div> -->
    <section class="checkout-section spad">
        <div class="container">
            <form  class="checkout-form" id="rzp-footer-form" action="{!!route('dopayment')!!}" method="POST" style="width: 100%; text-align: center" >
                @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="checkout-content">
                            <a href="#" class="content-btn">Click Here To Login</a>
                        </div>
                        <h4>Biiling Details</h4>                                                
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="fir">First Name<span>*</span></label>
                                <input type="text" value="" id="fir" name="firstname" required>
                            </div>
                            <div class="col-lg-6">
                                <label for="last">Last Name<span>*</span></label>
                                <input type="text" value="" id="last" name="lastname" required>
                            </div>
                            <div class="col-lg-12">
                                <label for="cun-name">Phone Number</label>
                                <input type="number" value="" id="cun-name" name="phone" required>
                            </div>
                            <div class="col-lg-12">
                                <label for="cun">Country<span>*</span></label>
                                <input type="text" id="cun" value="" name="country" required>
                            </div>
                            <div class="col-lg-12">
                                <label for="street">Street Address<span>*</span></label>
                                <input type="text" id="street" value="" class="street-first" name="street">
                                <label for="street">Landmark<span>*</span></label>
                                <input type="text" name="landmark" value="" id="landmark" required>
                            </div>
                            <div class="col-lg-12">                                
                                <label for="zip">Postcode / ZIP (optional)</label>
                                <input type="number" id="zip" value="" name="zipcod" required>
                            </div>
                            <div class="col-lg-12">
                                <label for="town">Town / City<span>*</span></label>
                                <input type="text" id="town" value="" name="city" required>
                            </div>
                            <div class="col-lg-6">
                                <label for="email">Email Address<span>*</span></label>
                                <input type="email" id="email" value="" name="email" required>
                            </div>                            
                            <div class="col-lg-12">
                                <div class="create-item">
                                    <label for="acc-create">
                                        Create an account?
                                        <input type="checkbox" id="acc-create">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="checkout-content">
                            <input type="text" placeholder="Enter Your Coupon Code" name="cupen">
                        </div>
                        <div class="place-order">
                            <h4>Your Order</h4>
                            <div class="order-total">
                                <ul class="order-table">
                                    <li>Product <span>Total</span></li>
                                    @foreach($cart as $cart)
                                    <li class="fw-normal"><b>{{$cart->product_name}} x {{$cart->qty}}</b><span>RS:{{$cart->price}}/-</span></li>
                                    <input type="number" id="product_id" name="cart_id" value="{{$cart->id}}" hidden>
                                    <input type="number" id="product_id" name="product_id" value="{{$cart->product_id}}" hidden>
                                    <input type="number" id="product_id" name="qty" value="{{$cart->qty}}" hidden>
                                    @endforeach
                                    <li class="total-price">Total <span>RS:{{$total}}/-</span></li>
                                    <input type="number" id="total_amount" name="amount" value="{{$total}}" hidden>
                                </ul>                 
                                <h3 class="">Choose Payment Option</h3><br/>                                
                                    <label class="radio-inline">
                                      <input type="radio" name="payment" id="payment1" value="upi" checked>UPI
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="payment" id="payment2" value="online">ONLINE
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="payment" id="payment3" value="cod">COD
                                    </label>                            
                            </div>
                            <div id="online-payment-popup">
                                
                            </div>                                                    
                        </div>                                                                        
                        <br/><br/>
                        <div class="pay">
                           <button class="razorpay-payment-button btn filled small site-btn place-btn" id="paybtn" type="submit">Place Order</button>                        
                        </div>
                        <div id="paymentDetail" style="display: none">
                            <center>
                                <div>paymentID: <span id="paymentID"></span></div>
                                <div>paymentDate: <span id="paymentDate"></span></div>
                            </center>
                        </div> 
                    </div>
                </div>
            </form>
        </div>
    </section>
    <!-- Shopping Cart Section End -->
    </body>         
<script>
    function padStart(str) {
        return ('0' + str).slice(-2)
    }

    function demoSuccessHandler(transaction) {
        
        $("#paymentDetail").removeAttr('style');
        $('#paymentID').text(transaction.razorpay_payment_id);
        var paymentDate = new Date();
        $('#paymentDate').text(
                padStart(paymentDate.getDate()) + '.' + padStart(paymentDate.getMonth() + 1) + '.' + paymentDate.getFullYear() + ' ' + padStart(paymentDate.getHours()) + ':' + padStart(paymentDate.getMinutes())
                );        
        swal("Good job!", "Payment Success! Your Order Was Placed", "success");
        $.ajax({
            method: 'post',
            url: "{!!route('dopayment')!!}",
            data: {
                "_token": "{{ csrf_token() }}",
                "razorpay_payment_id": transaction.razorpay_payment_id
            },
            complete: function (r) {
                console.log('complete');
                console.log(r);
            }
        })
    }    
</script>
<script>
    var options = {
        key: "{{ env('RAZORPAY_KEY') }}",
        amount: '{{$total}}',
        name: 'SinghSolution',
        description: 'Batter Life Batter Idia',
        image: '{{url("images/img/blog/recent-2.jpg")}}',
        handler: demoSuccessHandler        
    }
</script>
<script>
    window.r = new Razorpay(options);
    // document.getElementById('paybtn').onclick = function () {
    //       if (document.getElementById('payment1').checked) 
    //       {
    //             value = document.getElementById('payment1').value;
    //             r.open();
    //        }else if(document.getElementById('payment2').checked) 
    //        {
    //             value = document.getElementById('payment2').value;
    //             r.open();
    //        }else if (document.getElementById('payment3'))
    //        {
    //          value = document.getElementById('payment3').value;
    //          alert(value);
    //        }        
    // }    
    function payment()
    {
       if (document.getElementById('payment1').checked) 
        {
          value = document.getElementById('payment1').value;
          return true;           
       }else if(document.getElementById('payment2').checked) 
       {
          value = document.getElementById('payment2').value;
          return true;
       }else if (document.getElementById('payment3'))
       {
         value = document.getElementById('payment3').value;
         return false;
       }        
    }
</script>
<script>    
    $('#rzp-footer-form').submit(function (e) {
        var value = payment();
        if(value == true)
        {
            r.open();
        }
        else
        {
            alert('cod')
            var button = $(this).find('button');
            var parent = $(this);
            button.attr('disabled', 'true').html('Please Wait...');
            $.ajax({
                method: 'POST',
                url: this.action,
                data: $(this).serialize(),
                success: function (r) {
                    console.log('complete');
                    window.location.href = "myorder";                     
                },
                error:function(XMLHttpRequest, textStatus, errorThrown)
                {
                   alert("Status: " + textStatus); alert("Error: " + errorThrown); 
                }
            })
        }        
        return false;
    })
</script>
@endsection
