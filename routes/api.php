<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



	Route::post('login', 'apiController@login');
    Route::post('signup', 'apiController@signup');
    Route::post('sendotp', 'apiController@Sendotp');        
    Route::post('forgotpassword', 'apiController@forgotpassword');        

    Route::group(['middleware' => 'auth:api'],function() {
        Route::get('logout', 'apiController@logout');
        Route::get('user', 'apiController@user');
        Route::post('post', 'apiController@post');
        Route::get('getpost', 'apiController@getPost');
        Route::get('profile', 'apiController@profile');
        Route::post('delete', 'apiController@postDelete');
        Route::post('edit', 'apiController@edit_post');		
        Route::get('M_otp', 'apiController@M_otp');		
        Route::post('mobileVerified', 'apiController@mobileVerified');		
        Route::post('mail', 'apiController@mail');	       
        Route::post('comment', 'apiController@passComments');       
        Route::get('rolecreate', 'apiController@rolecreate');
        Route::get('getrole', 'apiController@getrole');       
    });

