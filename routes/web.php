<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/	

Route::get('/', function () {
    return view('index');
});
Route::get('/','HomeController@index');
Auth::routes(['verify'=>true]);
Route::get('/home', 'HomeController@index')->name('home');
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('/notify','HomeController@notify');
Route::get('/markAsRead','HomeController@markAsRead')->name('mark');
Route::view('shop', 'shop');
Route::get('/product/{id}', 'ShopController@product')->name('admin');
Route::post('/cart', 'CartController@Add')->name('admin');
Route::get('cart-load', 'CartController@getcart')->name('admin');
Route::get('/viewcart', 'CartController@cartview');
Route::post('deletecart', 'CartController@deletecart');
Route::post('/updatecart', 'CartController@updatecart');
Route::get('gettab/{name}', 'HomeController@gettab');
Route::get('pay', 'RazorpayController@pay')->name('pay');
Route::post('dopayment', 'RazorpayController@dopayment')->name('dopayment');
Route::get('myorder', 'HomeController@myorder');
Route::view('admin', 'Admin\login')->name('admin');
Route::post('admin/login', 'Admin\adminController@login');

Route::get('ordertrack/{id}', 'HomeController@ordertrack');

Route::post('getstatus', 'HomeController@getstatus');

Route::view('admin/home', 'Admin\index')->name('admin');

Route::get('admin/logout', 'Admin\adminController@logout');

Route::get('admin/products', 'Admin\ProductController@index');

Route::post('admin/subcat', 'Admin\subcatController@index');

Route::post('admin/add/product', 'Admin\productController@Add');

Route::post('admin/add/category', 'Admin\CategoryController@Add');

Route::view('admin/category', 'Admin\category',)->name('admin');

Route::get('admin/category', 'Admin\CategoryController@index');

Route::view('admin/subcategory', 'Admin\subcategory',)->name('admin');

Route::get('admin/subcategory', 'Admin\subcatController@subcat');

Route::view('admin/brand', 'Admin\brand',)->name('admin');

Route::get('admin/brand', 'Admin\brandController@index');

Route::post('admin/upbrand', 'Admin\brandController@getdata');

Route::post('admin/update/Brand', 'Admin\brandController@update');

Route::post('admin/upcategory', 'Admin\CategoryController@getdata');

Route::post('admin/update/category','Admin\CategoryController@update');

Route::post('admin/upsubcategory', 'Admin\subcatController@getdata');

Route::post('admin/update/subcategory','Admin\subcatController@update');

Route::post('admin/upproducts', 'Admin\ProductController@getdata');

Route::post('admin/update/products','Admin\ProductController@update');

Route::view('admin/add/slider', 'Admin\addslider',)->name('admin');

Route::post('admin/add/sliderc', 'Admin\DesignController@AddSlider',);

Route::view('admin/add/sideimg', 'Admin\sideimg',)->name('admin');

Route::post('admin/add/sideimgc', 'Admin\DesignController@AddSideimg',);

Route::view('admin/addbrand', 'Admin\addbrand')->name('admin');

Route::post('admin/add/brand', 'Admin\BrandController@Add',);

Route::view('admin/addsubcat', 'Admin\addsubcat')->name('admin');

Route::get('admin/addsubcat', 'Admin\CategoryController@getcat');

Route::post('admin/add/subcat', 'Admin\subcatController@Add');

Route::view('admin/addproduct', 'Admin\addproduct')->name('admin');

Route::get('admin/addproduct', 'Admin\productController@getproduct');

Route::view('admin/addcategory', 'Admin\addcategory')->name('admin');

Route::view('admin/dealofweek', 'Admin\dealofweek')->name('admin');

Route::get('admin/dealofweek', 'Admin\productController@getallproducts');

Route::post('admin/add/dealofweek', 'Admin\DealofWeak@Add');

Route::view('admin/post', 'Admin\post')->name('admin');

Route::get('admin/post', 'Admin\PostController@getdata');

Route::view('admin/addpost', 'Admin\addpost')->name('admin');

Route::post('admin/add/post', 'Admin\PostController@post');

Route::get('admin/Orders', 'Admin\ProductController@Orders');

Route::post('admin/update/orderstatus', 'Admin\ProductController@orderstatus');

Route::get('admin/productdetails/{id}', 'Admin\ProductController@productdetails');



