<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\Newmail;
use Carbon\Carbon;
use App\User;
use App\Post;
use App\otp;
use App\Comment;
use App\Role;
use App\Role_user;
use Validator;
use Notification;
use App\Notifications\MyNotification;

class apiController extends Controller
{
      public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'phone_no' => 'required',
            'password' => 'required|string'
        ]);
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'phon_no' =>$request->phone_no,
            'password' => bcrypt($request->password)
        ]);
        $user->save();
        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }
  
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {    	
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',            
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }
  
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
  
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function post(Request $request)
    {
    	$validate = Validator::make($request->all(),[
            'image' =>'required',
            'caption'=>'required'            
        ]);
        if($validate->fails())
        {
            $validator = $validate->errors();
            return response()->json([
                'responsecode'=>'203',
                'massege'=>$validator
                ]);
        }
        else
        {
        	 $imageName = time().'.'.$request->image->extension();  
        	 $request->image->move(public_path('images'), $imageName);        	 
        	 $image = new Post;
        	 $image->image = $imageName;
        	 $image->caption = $request->input('caption');
        	 $image->user_id = auth()->user()->id;
        	 $image->save();
             return response()->json(['success'=>'You have successfully upload image.']);
        }
    }

    public function getPost()
    {
    	$post=Post::with(['user','comment'])->withcount('comment')->get();
    	$comment = Comment::with(['Cuser'])->get();    	
    	return response()->json(['post'=>$post,'Commented_user'=>$comment]);
    }
    public function profile()
    {    	
    	$id = auth()->user()->id;
    	$post = Post::where('user_id',$id)->get();
    	$count=$post->count();
    	return response()->json(['post'=>$post,'post-count'=>$count]);
    }
    public function postDelete(Request $request)
    {
    	$id = $request->id;    	
    	$user_id = auth()->user()->id;
    	if(Post::where('id',$id)->Where('user_id',$user_id)->delete())
    	{
    		return response()->json(['success'=>'post Delete successfully']);
    	}    	        	
    	else
    	{
    		return response()->json(['success'=>'post cant find']);
    	}
    }
    public function edit_post(Request $request)
    {
    	$id = $request->id;
    	$post = Post::where('id',$id)->get();    	
    	if(!empty($post))
    	{	
    		$image = $request->file('image');    		
    		$caption = $request->input('caption');
    	    if(!empty($image))
    	    {	   
			  $imageName = time().'.'.$request->image->extension();  			  
			  $request->image->move(public_path('images'), $imageName);        	 
             }
             else
             {
             	$imageName = $post[0]->image;              	             	             
             }
             $value=array();
             $value['image']= $imageName;
             $value['caption'] = (!empty($caption)) ? $caption:$post[0]->caption;	  
             $update = Post::whereId($id)->update($value);
	         return response()->json(['success'=>'You have successfully Update post.']);
    	}
    }
    public function Sendotp(Request $request)
    {
    		$basic  = new \Nexmo\Client\Credentials\Basic('2d1868b7', 'rRVb34w2k04scefc');
			$client = new \Nexmo\Client($basic);
			$otp = rand(10,10000);
			$phon = $request->phone_no;					
			$getuser = User::where('phone_no',$phon)->first();			
			if(empty($getuser))
			{
				return response()->json(['verefy'=>'this Account was not found']);
			}
			else
			{
					$getuser->otp = $otp;										
					$getuser->save();					
					$message = $client->message()->send([
			    	'to' => '916378707597',
				    'from' => 'Balwant',
				    'text' =>$otp				    
				]);
			}						
    }
    public function forgotpassword(Request $request)
    {
		$validate = Validator::make($request->all(),[
            'otp' =>'required',
            'password'=>'required',
            'c_password'=>'required',
            'phone_no'=>'required'           
        ]);
        if($validate->fails())
        {
            $validator = $validate->errors();
            return response()->json([
                'responsecode'=>'203',
                'massege'=>$validator
                ]);
        } 
        else
        {
        	$otp = $request->otp;        	        	
        	$oldOtp = User::where('phone_no',$request->phone_no)->first();
        	if($otp == $oldOtp->otp)
        	{
        		$oldOtp->password = bcrypt($request->password);
        		$oldOtp->otp = null;
        		$oldOtp->save();
        		return response()->json(['success'=>'Password changed successfully']);
        	}
        	else
        	{
        		return response()->json(['error'=>'Otp dose not Match']);
        	}
        }  	    	
    }
    public function M_otp()
    {
    		$basic  = new \Nexmo\Client\Credentials\Basic('2d1868b7', 'rRVb34w2k04scefc');
			$client = new \Nexmo\Client($basic);
			$otp = rand(10,10000);
			$id = auth()->user()->id;					
			$getuser = User::find($id);			
			if(empty($getuser))
			{
				return response()->json(['verefy'=>'this Account was not found']);
			}
			else
			{
					$getuser->otp = $otp;										
					$getuser->save();					
					$message = $client->message()->send([
			    	'to' => '916378707597',
				    'from' => 'Balwant',
				    'text' =>$otp				    
				]);
			}						
    }
    public function mobileVerified(Request $request)
    {
    	$validate = Validator::make($request->all(),[
            'otp' =>'required',                        
        ]);
        if($validate->fails())
        {
            $validator = $validate->errors();
            return response()->json([
                'responsecode'=>'203',
                'massege'=>$validator
                ]);
        } 
        else
        {
        	$otp = $request->otp;
        	$id = auth()->user()->id;         	        	
        	$oldOtp = User::find($id);        			
        	if($otp == $oldOtp->otp)
        	{        		
        		$oldOtp->otp = null;
        		$oldOtp->mobile_verified_at = Carbon::now();        		
        		$oldOtp->save();
        		return response()->json(['success'=>'Mobile Verified successfully']);
        	}
        	else
        	{
        		return response()->json(['error'=>'Otp dose not Match']);
        	}
        }
    }
    public function mail(Request $request)
    {
    	$validate = Validator::make($request->all(),[
            'email' =>'required',
            'message'=>'required',            
        ]);
        if($validate->fails())
        {
            $validator = $validate->errors();
            return response()->json([
                'responsecode'=>'203',
                'message'=>$validator
                ]);
        }
        else
        {
        	$data = array(
        		'message'=>$request->message,
        		'email' =>$request->email      		
        	);
        	Mail::to('websenor.balwant@gmail.com')->send(new Newmail($data));
        	return response()->json(['success'=>'Mail Send Successfully']);
        }
    }
    public function passComments(Request $request)
    {
    	$validate = Validator::make($request->all(),[
            'comments' =>'required',                                   
        ]);
        if($validate->fails())
        {
            $validator = $validate->errors();
            return response()->json([
                'responsecode'=>'203',
                'massege'=>$validator
                ]);
        } 
        else
        {
        	$comment = new Comment;
        	$comment->comments = $request->comments;
        	$comment->post_id  = $request->post_id;
        	$comment->user_id  = auth()->user()->id;
        	$comment->save();
        	return response()->json(['success'=>'Comment Successfully']);
        }
    }    
    public function rolecreate()
    {
    	$role = new Role;
        $role->name = 'Superadmin';
        $role->save();

        $user = User::find([5,6]);
        $role->user()->attach($user);
        return response()->json(['success'=>'success']);

    }
    public function getrole()
    {
        $id = auth()->user()->id;        
        $role = User::find($id)->role();        
        return response()->json(['role'=>$role]);
    }
}
