<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Razorpay\Api\Api;
use App\Cart;
use App\Order;
use App\Oreder_address;
use App\Payment;
use Carbon\Carbon;
use App\User;
use App\Notifications\OrderNotification;
use Illuminate\Support\Facades\Mail;
use App\Mail\Newmail;

class RazorpayController extends Controller {

    public function pay() {

    	$id = auth()->user()->id;
    	$cart = Cart::with('product')->where('user_id',$id)->get();
    	$total = $cart->sum('price');                               
    	return view('check-out',['cart'=>$cart,'total'=>$total]);        
    }

    public function dopayment(Request $request) {        
       // $payment_api = new Api(env('RAZORPAY_KEY', 'rzp_test_Gs8a5mxc82IKyH'), env('RAZORPAY_SECRET', 'RyGZMun9Hq2WhQoUHPZKFB99'));     
       //  $payment = $payment_api->payment->fetch($request->input("payment"))->capture(array('amount' => $request->input("amount")));        
       // return $payment;    	
       if($request->get('payment') == 'cod')
       {              	 
       	   $delivery_date = Carbon::now();
       	   $delivery_date->addDays(5);


           $order = Order::create([
               'user_id' => auth()->user()->id,
		       'payment_id' => $request->input('payment'),
		       'gross_price' => $request->input('amount'),
		       'delivery_carg' => '0',
		       'status' => 'processing',
		       'delivery_date' => $delivery_date,
		       'product_id' => $request->input('product_id'),
		       'qty' => $request->input('qty'),
           ]);
           $oId = $order->id;           	       

	       $order_adress = new Oreder_address;
	       $order_adress->firstname = $request->input('firstname');
	       $order_adress->lastname = $request->input('lastname');
	       $order_adress->phon = $request->input('phone');
	       $order_adress->country = $request->input('country');
	       $order_adress->street = $request->input('street');
	       $order_adress->city = $request->input('city');
	       $order_adress->landmark = $request->input('landmark');	       
	       $order_adress->email =$request->input('email');	       
	       $order_adress->order_id = $oId;
	       $order_adress->zip = $request->input('zipcod');
	       $order_adress->save();

	       $payment = new Payment;
	       $payment->order_id = $oId;
	       $payment->transiction_id = 'NULL';
	       $payment->via = 'NULL';
	       $payment->method = $request->input('payment');
	       $payment->amount = $request->input('amount');
	       $payment->save();

	       Cart::find($request->input('cart_id'))->delete();	       
	       $id = auth()->user()->id;
           $user = User::find($id);
           $details = [                
                'notification'=>'New Order Was Placed Go To Order Page And See Details'
           ];
           $user->notify(new OrderNotification($details));
           $email = $request->input('email');
           $data = array(
                'logo'=>'https://us.123rf.com/450wm/lumut/lumut1505/lumut150500012/39994162-stock-vector-s-lame-logo.jpg?ver=6',           	    
        		'address'=>$request->input('street'),
        		'date'=>$delivery_date,        		      	       	
        	);
        	Mail::to($email)->send(new Newmail($data));
	       return array('orderid'=>$oId,'date'=>$delivery_date);
       }	       

    //     $input = $request->all();        
    //     return  json_encode($input);
    //     exit;
    }
}