<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\timedeal;
use App\Product;
use App\Brand;
use App\Category;
use App\SubCategory;
use App\Sideimage;
use App\Slider;
use App\Post;

class DealofWeak extends Controller
{
    public function Add(Request $request)
    {         
      $request->validate([            
          'start_time'=>'required',
          'end_time'=>'required',
          'd_product'=>'required',
          'd_content'=>'required',
          'img'=>'required|image|mimes:jpeg,png,jpg,svg',
       ]);
         $start_date = $request->start_time;
	     $startdate = Carbon::parse($start_date)->format('Y-m-d g:i:s');
	     $end_date = $request->end_time;
	     $enddate = Carbon::parse($end_date)->format('Y-m-d g:i:s');     

         $dealofWeak = new timedeal;         
         $dealofWeak->start_date = $startdate;
         $dealofWeak->end_date = $enddate;
         $dealofWeak->product_id = $request->input('d_product');
         $dealofWeak->content = $request->input('d_content');
         $files = $request->file('img');
         $destinationPath = 'images/dealofWeak/'; // upload path
         $profileImage =$destinationPath.time().".".$files->getClientOriginalExtension();
         $files->move($destinationPath, $profileImage);
         $dealofWeak->image = $profileImage;       
         $dealofWeak->save();
         $request->session()->flash('status','DealofWeak Update Successfully');
	     return redirect('admin/home');      
    }   
}
