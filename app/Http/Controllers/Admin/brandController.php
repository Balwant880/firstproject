<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Brand;
use validator;

class brandController extends Controller
{
    public function index(Request $request)
    {    	
       $brand = Brand::all();              	       
       return view('Admin\brand',['brand'=>$brand,'title'=>'Brand']);
    }    
    public function getdata(Request $request)
    {
    	$id = $request->get('id');
    	$brand = Brand::where('id',$id)->get(); 
        return response()->json(['brand'=>$brand]);
    }
    public function Add(Request $request)
    {    	
      $request->validate([            
          'b_name'=>'required',
          'b_details'=>'required',
          'b_img'=>'required',
       ]);
	   $brand = new Brand;
	   $brand->name = $request->input('b_name');
	   $brand->details = $request->input('b_details');
	   $files = $request->file('b_img');
	   $destinationPath = 'images/brandimg/'; // upload path
	   $profileImage =$destinationPath.time().".".$files->getClientOriginalExtension();
	   $files->move($destinationPath, $profileImage);  
	   $brand->image = $profileImage;     
	   $brand->save();
	    $request->session()->flash('status','Brand Add Successfully');
        return redirect('admin/brand');      
    }
    public function update(Request $request)
    {      	      
       $request->validate([            
          'name'=>'required',
          'details'=>'required',
       ]);
       $brand = Brand::where('id',$request->input('id'))->first();
       if(!empty($request->file('img')))
       {
          $request->validate([            
            'img'=>'image|mimes:jpeg,png,jpg,svg',            
        ]);          
	       $files = $request->file('img');
	       $destinationPath = 'images/'; // upload path
	       $profileImage =$destinationPath.time().".".$files->getClientOriginalExtension();
	       $files->move($destinationPath, $profileImage);  
	       $brand->image = $profileImage;     
       }        
        $brand->name = $request->input('name');
        $brand->details = $request->input('details');
        $brand->save();
        $request->session()->flash('status','Brand Update Successfully');
        return redirect('admin/brand');      
    }    
}
    
