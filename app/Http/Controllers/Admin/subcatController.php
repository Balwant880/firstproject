<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SubCategory;
use App\Category;

class subcatController extends Controller
{
    public function index(Request $request)
    {
       $cat_id = $request->get('cat_id');       
       $subcat = SubCategory::where('parent_id',$cat_id)->get(); 
       return response()->json(['subcate'=>$subcat]);
    }
    public function subcat()
    {
    	$subcategory = SubCategory::with('category')->get();              	
        return view('Admin\subcategory',['subcategory'=>$subcategory,'title'=>'Subcategory']);
    }
     public function getdata(Request $request)
    {    	
    	$id = $request->get('id');
    	$subcategory = SubCategory::with('category')->where('id',$id)->get()->toArray();    	
    	$allcat = Category::all();
        return response()->json(['subcategory'=>$subcategory,'allcat'=>$allcat]);
    }
    public function Add(Request $request)
    {
       $request->validate([            
          'sub_name'=>'required',
          'parent_cat'=>'required',
          'stetus'=>'required',
       ]); 
       $subcat = new SubCategory;
       $subcat->name = $request->input('sub_name');
       $subcat->parent_id = $request->input('parent_cat');
       $subcat->active = $request->input('stetus');
       $subcat->save();
       $request->session()->flash('status','subcategory Add Successfully');
       return redirect('admin/subcategory');      
    }

     public function update(Request $request)
    {      	              
       $request->validate([            
          'name'=>'required',
          'category'=>'required',
          'stetus'=>'required',
       ]);
       $subcategory = SubCategory::where('id',$request->input('id'))->first();       
       $subcategory->name = $request->input('name');
       $subcategory->parent_id = $request->input('category');
       $subcategory->active = $request->input('stetus');
       $subcategory->save();
       $request->session()->flash('status','subcategory Update Successfully');
       return redirect('admin/subcategory');      
    }
}
