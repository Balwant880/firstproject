<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\admin;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\Newmail;
use Carbon\Carbon;
use App\User;
use App\Post;
use App\otp;
use App\Comment;
use App\Role;
use App\Role_user;
use Validator;
use Notification;
use App\Notifications\MyNotification;
use illuminate\Support\Arr;

class adminController extends Controller
{
    public function login(Request $request)
    {       
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|min:8',            
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
        {   
            $request->session()->flash('status','Email & Password Was Inncorect');         
            return redirect('admin');
        }
        else
        {
            $user = $request->user();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            if ($request->remember_me)
            {
                $token->expires_at = Carbon::now()->addWeeks(1);
                $token->save();
            }            
               $role = $user->role[0]->name;
               if($role == 'Superadmin')
                {
                  return redirect('admin/home');
                }
                else
                {
                    $request->session()->flash('status','You are Not a Admin');         
                    return redirect('admin');
                }     
            }
        }                            
         public function logout(Request $request)
        {            
            Auth::logout();
            $request->session()->flash('status','You are logout Successfully');         
            return redirect('admin');
        }
    }

