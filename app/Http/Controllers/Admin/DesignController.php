<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Slider;
use App\Sideimage;

class DesignController extends Controller
{
    public function index()
    {
    	//
    }
    public function AddSlider(Request $request)
    {         
     
      if($request->input('S_text1'))
      {
      	   $request->validate([            
	          'S_text1'=>'required',
	          'S_content1'=>'required',
	          'S_img1'=>'required',
           ]);
           $slider = Slider::where('id',1)->first();
	       $slider->s_text1 = $request->input('S_text1');
	       $slider->s_content1 =$request->input('S_content1');
	       $files = $request->file('S_img1');
	       $destinationPath = 'images/slider/'; // upload path
	       $profileImage =$destinationPath.time().".".$files->getClientOriginalExtension();
	       $files->move($destinationPath, $profileImage);       
	       $slider->s_img1 = $profileImage; 
	       $slider->save();
	       $request->session()->flash('status','Slider 1 Update Successfully');
	       return redirect('admin/home');      
      }   
      elseif($request->input('S_text2'))
      {
      	   $request->validate([            
	          'S_text2'=>'required',
	          'S_content2'=>'required',
	          'S_img2'=>'required',
           ]);           
           $slider = Slider::where('id',1)->first();
           $slider->s_text2 = $request->input('S_text2');       
	       $slider->s_content2 = $request->input('S_content2');     
	       $files = $request->file('S_img2');
	       $destinationPath = 'images/slider/'; // upload path
	       $profileImage =$destinationPath.time().".".$files->getClientOriginalExtension();
	       $files->move($destinationPath, $profileImage);       
	       $slider->s_img2 = $profileImage; 
	       $slider->save();
	       $request->session()->flash('status','Slider 2 Update Successfully');
	       return redirect('admin/home');      
      }      
      else
      {      	
      	$request->session()->flash('error','Slider Canot Update');      
      	return redirect('admin/home');
      }              
    }
    public function AddSideimg(Request $request)
    {
       if($request->file('l_img'))
       {
           $sideimg = Sideimage::where('id',1)->first();
           $files = $request->file('l_img');
	       $destinationPath = 'images/sideimg/'; // upload path
	       $profileImage =$destinationPath.time().".".$files->getClientOriginalExtension();
	       $files->move($destinationPath, $profileImage);       
	       $sideimg->l_img = $profileImage;
	       $sideimg->save();
	       $request->session()->flash('status','Left Side image Update Successfully');
	       return redirect('admin/home');       
       }   
       elseif($request->file('r_img'))    
       {
       	   $sideimg = Sideimage::where('id',1)->first();
           $files = $request->file('r_img');
	       $destinationPath = 'images/sideimg/'; // upload path
	       $profileImage =$destinationPath.time().".".$files->getClientOriginalExtension();
	       $files->move($destinationPath, $profileImage);       
	       $sideimg->r_img = $profileImage;
	       $sideimg->save();
	       $request->session()->flash('status','Right Side image Update Successfully');
	       return redirect('admin/home');       
       }
       else
       {
	       	$request->session()->flash('error','Side Image Canot Update');      
	      	return redirect('admin/home');
       }
    }
}
