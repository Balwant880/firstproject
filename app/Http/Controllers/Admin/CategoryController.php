<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
      $category = Category::all();            
      return view('Admin\category',['category'=>$category,'title'=>'Category']);
    }

    public function Add(Request $request)
    {
    	$request->validate([
            'name' => 'required|string',                       
            'img' => 'required|image|mimes:jpeg,png,jpg,svg',            
        ]);
       $category = new Category;
       $category->name = $request->input('name');
       $files = $request->file('img');
       $destinationPath = 'images/'; // upload path
       $profileImage =$destinationPath.time().".".$files->getClientOriginalExtension();
       $files->move($destinationPath, $profileImage);       
       $category->image = $profileImage;
       $category->active = $request->input('stetus');
       $category->save();
       $request->session()->flash('status','Category Added Successfully');
       return redirect('admin/category');      
    }
     public function getdata(Request $request)
    {    	
    	$id = $request->get('id');
    	$category = Category::where('id',$id)->get(); 
        return response()->json(['category'=>$category]);
    }
     public function update(Request $request)
    {      	          
       $request->validate([            
          'name'=>'required',
          'stetus'=>'required',
       ]);
       $category = Category::where('id',$request->input('id'))->first();
       if(!empty($request->file('img')))
       {
          $request->validate([            
            'img'=>'image|mimes:jpeg,png,jpg,svg',            
        ]);          
	       $files = $request->file('img');
	       $destinationPath = 'images/'; // upload path
	       $profileImage =$destinationPath.time().".".$files->getClientOriginalExtension();
	       $files->move($destinationPath, $profileImage);  
	       $category->image = $profileImage;     
       }        
        $category->name = $request->input('name');
        $category->active = $request->input('stetus');
        $category->save();
        $request->session()->flash('status','category Update Successfully');
        return redirect('admin/category');      
    }
    public function getcat()
    {
    	$category = Category::all();
    	return view('Admin\addsubcat',['category'=>$category]);
    }
}
