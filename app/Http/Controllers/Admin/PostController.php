<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use App\Role;
use App\Role_user;
use Validator;

class PostController extends Controller
{
     public function post(Request $request)
    {
    	$validate = Validator::make($request->all(),[
            'post' =>'required|image',
            'title'=>'required',
            'content'=>'required'            
        ]);
        if($validate->fails())
        {            
            $request->session()->flash('status','fill the corectly');
            return redirect('admin/addpost');
        }
        else
        {
        	 $files = $request->file('post');
			 $destinationPath = 'images/brandimg/'; // upload path
			 $profileImage =$destinationPath.time().".".$files->getClientOriginalExtension();
			 $files->move($destinationPath, $profileImage);  
        	 $post = new Post;
        	 $post->image = $profileImage;
        	 $post->content = $request->input('content');
        	 $post->caption = $request->input('title');
        	 $post->user_id = auth()->user()->id;
        	 $post->save();
             $request->session()->flash('status','Post Add Successfully');
             return redirect('admin/post');
        }
    }

    public function getPost()
    {
    	$post=Post::with(['user','comment'])->withcount('comment')->get();
    	$comment = Comment::with(['Cuser'])->get();    	
    	return response()->json(['post'=>$post,'Commented_user'=>$comment]);
    }
    public function profile()
    {    	
    	$id = auth()->user()->id;
    	$post = Post::where('user_id',$id)->get();
    	$count=$post->count();
    	return response()->json(['post'=>$post,'post-count'=>$count]);
    }
    public function postDelete(Request $request)
    {
    	$id = $request->id;    	
    	$user_id = auth()->user()->id;
    	if(Post::where('id',$id)->Where('user_id',$user_id)->delete())
    	{
    		return response()->json(['success'=>'post Delete successfully']);
    	}    	        	
    	else
    	{
    		return response()->json(['success'=>'post cant find']);
    	}
    }
    public function edit_post(Request $request)
    {
    	$id = $request->id;
    	$post = Post::where('id',$id)->get();    	
    	if(!empty($post))
    	{	
    		$post = $request->file('post');    		
    		$caption = $request->input('caption');
    	    if(!empty($post))
    	    {	   
			  $postName = time().'.'.$request->post->extension();  			  
			  $request->post->move(public_path('posts'), $postName);        	 
             }
             else
             {
             	$postName = $post[0]->post;              	             	             
             }
             $value=array();
             $value['post']= $postName;
             $value['caption'] = (!empty($caption)) ? $caption:$post[0]->caption;	  
             $update = Post::whereId($id)->update($value);
	         return response()->json(['success'=>'You have successfully Update post.']);
    	}
    }
    public function getdata()
    {
    	$post = Post::all();
    	return view('Admin\post',['post'=>$post,'title'=>'Post']);
    }
}
