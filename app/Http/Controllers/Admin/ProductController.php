<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Brand;
use App\Category;
use App\SubCategory;
use App\Product_image;
use App\Order;
use App\Oreder_address;

class ProductController extends Controller
{
    public function __construct()
    {        

    }
    public function index()
    {    	
      $products = Product::with(['category','brand','subcategory'])->get();             
      $brand = Brand::all();
      $category = Category::all();                              
      return view('Admin\products',['products'=>$products,'brand'=>$brand,'category'=>$category,'title'=>'Products']);
    }
    public function Add(Request $request)
    {
      $request->validate([
            'name' => 'required|string',
            'category' => 'required',            
            'subcategory' => 'required',            
            'brand' => 'required|string',            
            'stetus' => 'required|string',            
            'info' => 'required|string',            
            'img' => 'required|image|mimes:jpeg,png,jpg,svg',
            'description' => 'required|string',            
            'keywords' => 'required|string',
            'price'=> 'required',            
        ]);               
       $product = new Product;
       $product->name = $request->input('name');
       $product->cat_id = $request->input('category');       
       $files = $request->file('img');
       $destinationPath = 'images/'; // upload path
       $profileImage =$destinationPath.time().".".$files->getClientOriginalExtension();
       $files->move($destinationPath, $profileImage);       
       $product->cover_photo = $profileImage;       
       $product->description = $request->input('description');
       $product->brand_id = $request->input('brand');
       $product->sub_cat_id = $request->input('subcategory');
       $product->active = $request->input('stetus');
       $product->product_keyword = $request->input('keywords');
       $product->additional_info = $request->input('info');
       $product->price = $request->input('price');       
       $product->save();       
        $files2 = $request->file('files');
        foreach($files2 as $file)
        {
          $name = md5(uniqid(rand(), true)) . '.' . $file->getClientOriginalExtension();
          $file->move(public_path("images/products"),$name);
          $images = 'images/products/' . $name;
          $productimg = new Product_image;
          $productimg->path = $images;
          $productimg->product_id = $product->id;
          $productimg->active = 'Y';
          $productimg->save();
        }
       $request->session()->flash('status','Product Added Successfully');
       return redirect('admin/products');      
    }
    public function getdata(Request $request)
    {     
      $id = $request->get('id');
      $products = Product::with(['category','brand','subcategory'])->where('id',$id)->get()->toArray();
      $brand = Brand::all();
      $category = Category::all();                              
      $subcategory = SubCategory::all();
      return response()->json(['products'=>$products,'brand'=>$brand,'category'=>$category,'subcategory'=>$subcategory]);
    }
     public function update(Request $request)
    {                         
       $request->validate([            
          'name'=>'required',
          'stetus'=>'required',
          'category' => 'required',
          'subcategory' => 'required',
          'info' => 'required',
          'description' => 'required',
          'keywords' => 'required',
          'brand' => 'required'
       ]);       
       $product = Product::where('id',$request->get('id'))->first();             
       if(!empty($request->file('img')))
       {
          $request->validate([            
            'img'=>'image|mimes:jpeg,png,jpg,svg',            
        ]);          
         $files = $request->file('img');
         $destinationPath = 'images/'; // upload path
         $profileImage =$destinationPath.time().".".$files->getClientOriginalExtension();
         $files->move($destinationPath, $profileImage);  
         $product->cover_photo = $profileImage;     
       }        
        $product->name = $request->input('name');
        $product->active = $request->input('stetus');
        $product->description = $request->input('description');
        $product->brand_id = $request->input('brand');
        $product->sub_cat_id = $request->input('subcategory');        
        $product->product_keyword = $request->input('keywords');
        $product->additional_info = $request->input('info');
        $product->cat_id = $request->input('category');       
        $product->save();
        $request->session()->flash('status','Product Update Successfully');
        return redirect('admin/products');      
    }
    public function getproduct()
    {
       $category = Category::all();
       $brand = Brand::all();
       return view('Admin\addproduct',['category' =>$category,'brand' =>$brand]);
    }
    public function getallproducts()
    {
      $products = Product::all();
      return view('Admin\dealofweek',['products' =>$products]);
    }  
    public function Orders()
    {
      $order = Order::with(['product','orderAddress'])->get();            
      return view('Admin\orderdetails',['title'=>'Orders','order'=>$order]);
    } 
    public function orderstatus(Request $request)
    {
       $order = Order::find($request->input('order_id'))->first();
       $order->status = $request->get('stetus');
       $order->update();
       $request->session()->flash('status','Status Update Successfully');
       return redirect('admin/Orders');      
    }
    public function productdetails($id)
    {
      $products = Product::where('id',$id)->with(['category','brand','subcategory'])->get();
      $brand = Brand::all();
      $category = Category::all();                              
      return view('Admin\productdetails',['products'=>$products,'brand'=>$brand,'category'=>$category,'title'=>'Products']);
    }
}
