<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Brand;
use App\Category;
use App\SubCategory;
use App\Cart;
use Auth;

class CartController extends Controller
{
    public function Add(Request $request)
    {
    	if(!Auth::check())
    	{
    	   return response()->json(['error'=>'Login Required For Add to cart product']);
    	}
    	else
    	{    	     	     
            $id = $request->input('product_id');
	        $qty = $request->input('qty');	        
	        $uid = auth()->user()->id;	   	        
	        $product = Product::where('id',$id)->first();   	                     	
	        $cart = new Cart;	        
	        $cart->product_name = $product->name;
	        $cart->user_id = $uid;
	        $cart->product_option = '2';
	        $cart->product_id = $id;
	        $cart->product_option_id = '2';	        
	        $cart->qty = $qty;
	        $cart->price = $product->price*$qty;
	        $cart->save();
	        return response()->json(['cart'=>$cart]);
    	}        
    }
    public function getcart()
    {

    	if(!Auth::check())
    	{
    	   return response()->json(['error'=>'Login Required For Add to cart product']);
    	}
    	else
    	{
    	   $uid = auth()->user()->id;
           $cart = Cart::with('product')->where('user_id',$uid)->get();
           $total = $cart->sum('price');           
           return response()->json(['cart'=>$cart,'total'=>$total]);
    	}
    }
    public function cartview()
    {
    	if(!Auth::check())
    	{
    		return redirect('login');
    	}
    	else
    	{
          $id = auth()->user()->id;
          $cart = Cart::with('product')->where('user_id',$id)->get();
          $total = $cart->sum('price');                               
          return view('shopping-cart',['cart'=>$cart,'total'=>$total]);
    	}    	
    }
    public function deletecart(Request $request)
    {
    	$id = $request->get('id');
    	$cart = Cart::find($id)->delete();    	    	
    }
    public function updatecart(Request $request)
    {   
       if($request->input('up_qty') == 0)
       {
       	 return response()->json(['error'=>'you can not fill quantity 0']);
       } 	
       else
       {
          $id = $request->get('up_id');
    	  $cart = Cart::where('id',$id)->first();  
    	  $product_id = $cart->product_id;
    	  $product = Product::where('id',$product_id)->first();
    	  $product_price = $product->price;
    	  $cart->qty = $request->input('up_qty');
    	  $cart->price = $request->input('up_qty')*$product_price;
    	  $cart->save();
       }    	
    }
    public function getcartchek()
    {
    	$id = auth()->user()->id;
    	$cart = Cart::with('product')->where('user_id',$id)->get();
    	$total = $cart->sum('price');                               
    	return view('check-out',['cart'=>$cart,'total'=>$total]);
    }
}
