<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Brand;
use App\Category;
use App\SubCategory;

class ShopController extends Controller
{
    public function product($id)
    {
    	$id = $id; 
    	$product = Product::with(['category','brand','product_img'])->where('id',$id)->get();
    	$category = Category::latest('updated_at')->offset(0)->limit(3)->get();
    	$cat = $product[0]->category[0]->id;
    	$rel_product = Product::where('cat_id',$cat)->get();
    	$brand = Brand::latest('updated_at')->offset(0)->limit(4)->get();
    	foreach($product[0]->product_img as $p)
    	{
    		$arr[]= $p->path;
    	}    	
    	return view('Product',['product'=>$product,'arr'=>$arr,'category'=>$category,'brand'=>$brand,'related'=>$rel_product]);    	
    }
    
}
