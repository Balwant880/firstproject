<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\timedeal;
use App\Product;
use App\Brand;
use App\Category;
use App\SubCategory;
use App\Sideimage;
use App\Slider;
use App\Post;
use App\Cart;
use App\Order;
use Notification;
use App\Notifications\MyNotification;
use MustVerifyEmail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {               
        
    }

    public function index()
    {  
        $data = timedeal::latest('created_at')->first();        
        $products = Product::with(['category','brand','subcategory'])->get();                     
        $category = Category::latest('updated_at')->offset(0)->limit(5)->get();
        $slider  = Slider::all(); 
        $sideimg = Sideimage::all();         
        $post = Post::withcount('comment')->latest('created_at')->offset(0)->limit(3)->get();
        return view('index',
                   [
                    'data'=>$data,
                    'products'=>$products,
                    'category'=>$category,
                    'slider'=>$slider,
                    'post'=>$post,
                    'sideimg'=>$sideimg,                    
                   ]);
    }
    public function notify() 
    {
        $id = auth()->user()->id;
        $user = User::find($id);
        $details = [
                'greeting' => 'Hi Artisan',
                'body' => 'This is our example notification tutorial',
                'thanks' => 'Thank you for visiting our website!',
                'subscription'=>'subscrib chanle'
        ];

        $user->notify(new MyNotification($details));
        return redirect('admin');
    }
    public function markAsRead()
    {
        $user = auth()->user()->unreadNotifications->markAsRead();        
        return redirect('admin');
    }  
    public function gettab($name)
    {        
      return $tabproduct = Product::with(['category','brand','subcategory'])->where('cat_id',$name)->get();   
    }
    public function myorder()
    {
       $id = auth()->user()->id;
       $order = Order::where('user_id',$id)->with(['product','orderAddress'])->get();
       $total = $order->sum('gross_price');      
       return view('myorder',['order'=>$order,'total'=>$total]);
    }
    public function ordertrack($id)
    {
       $order = Order::where('id',$id)->first();
       return view('ordertrack',['order'=>$order]);
    }
    public function getstatus(Request $request)
    {
       $id = $request->input('id');
       $order = Order::select('status')->where('id',$id)->first();       
       return $order;
    }
}
