<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class CustomAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {        
        // $role = User::select('is_admin')->where()->get();
        return $next($request);
    }

}
