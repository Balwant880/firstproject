<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    public function category()
    {
    	return $this->hasMany('App\Category','id','parent_id');
    }
}
