<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function brand()
    {
    	return $this->hasMany('App\Brand','id','brand_id');
    }
    public function category()
    {
    	return $this->hasMany('App\Category','id','cat_id');	
    }
     public function subcategory()
    {
    	return $this->hasMany('App\SubCategory','id','sub_cat_id');	
    }
    public function product_img()
    {
    	return $this->hasMany('App\Product_image','product_id','id');	
    }
}
