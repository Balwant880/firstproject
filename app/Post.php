<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Post extends Model
{
    public function user()
    {
    	return $this->hasOne('App\User','id','user_id');
    }
    public function comment()
    {
    	return $this->hasMany('App\Comment','post_id','id');
    }    
}
