<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\BroadcastMessage;

class MyNotification extends Notification
{
    
    use Queueable;

    private $details;
   
    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public function __construct($details)
    {
        $this->details = $details;
    }

    public function via($notifiable)
    {
         return ['mail','database','broadcast'];
    }

    public function toMail($notifiable)
    {
         return (new MailMessage)
                    ->greeting($this->details['greeting'])
                    ->line($this->details['body'])
                    ->line($this->details['thanks']);
                   
    }

    public function toDatabase($notifiable)
    {
        return [
           'data' => $this->details['body']
        ];
    }
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'data'=>$this->details['subscription'],
            'count'=>$notifiable->unreadNotifications->count(),
        ]);
    }
}
