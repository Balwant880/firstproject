<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
  
    public function boot()
    {
        // Add the following line
        Schema::defaultStringLength(191);
        date_default_timezone_set('Asia/Calcutta');
    }
}

