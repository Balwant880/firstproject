<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
   protected $guarded = [];   
   public function product()
   {
   	 return $this->hasMany('App\Product','id','product_id');	
   }
   public function orderAddress()
   {
   	 return $this->hasMany('App\Oreder_address','order_id','id');	
   }
}
