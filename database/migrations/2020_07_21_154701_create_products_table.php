<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {            
            $table->bigIncrements('id');
            $table->string('skucode');
            $table->integer('cat_id');
            $table->string('name');
            $table->string('cover_photo');
            $table->string('description');
            $table->integer('brand_id');
            $table->integer('sub_cat_id');
            $table->enum('active',['Y','N'])->default('Y');
            $table->string('product_keyword');
            $table->string('additional_info');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
